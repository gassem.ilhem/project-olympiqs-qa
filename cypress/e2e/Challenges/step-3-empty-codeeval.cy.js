describe("challenges", () => {
  let challenge;
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
      cy.fixture("challenge.json").then((data) => {
        challenge = data;
      });
    });
  });
  it("should check creat a challenge training codeeval step 3 empty", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createSkillApi().then(function (firstResponse) {
      cy.navigateToChallengesPage();

      cy.contains("Create").click();

      cy.url().should("include", "/create");

      cy.contains("Code eval").click();

      cy.contains("Training").click();
      cy.contains("Next").click();
      cy.get('[data-cy="title"]').type(
        challenge.challengeCodeEvalTraining.Title
      );
      cy.get('[data-cy="difficulty"]').select(
        challenge.challengeCodeEvalTraining.Difficulty
      );
      cy.get('[data-cy="duration"]').type(
        challenge.challengeCodeEvalTraining.Duration
      );
      cy.get('[data-cy="xpAtStake"]').type(
        challenge.challengeCodeEvalTraining.XPs
      );
      cy.get('[data-cy="repetitions"]').type(
        challenge.challengeCodeEvalTraining.numRepetitions
      );
      cy.get('[data-cy="search-undefined"]').type(firstResponse.body.name);
      cy.contains(firstResponse.body.name).click();
      cy.contains("Next").click();

      cy.contains("Next").click();
      cy.contains("This field is required").should("be.visible");
      cy.contains("at least 3 test cases are required.").should("be.visible");
    });
  });
});
