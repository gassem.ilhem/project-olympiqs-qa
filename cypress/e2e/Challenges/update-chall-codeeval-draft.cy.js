describe("challenges", () => {
  let challenge;
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
      cy.fixture("challenge.json").then((data) => {
        challenge = data;
      });
    });
  });
  it("should successfully update challenge codeeval published", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createDraftChallengeCodeEvalByApiWithoutBody().then(function (
      firstResponse
    ) {
      cy.navigateToChallengesPage();
      cy.wait(3000);
      cy.get("#pencil").click({ force: true });
      cy.location("pathname", { timeout: 60000 }).should("include", "/update");
      cy.contains("Competition").click();
      cy.contains("Next").click();
      cy.get("select#difficulty").select(
        challenge.updateChallengeCodeEvalDraft.Difficulty
      );
      cy.getByDataCy("duration").clear();
      cy.getByDataCy("duration").type(
        challenge.updateChallengeCodeEvalDraft.Duration
      );
      cy.getByDataCy("xpAtStake").clear();
      cy.getByDataCy("xpAtStake").type(
        challenge.updateChallengeCodeEvalDraft.XPs
      );

      cy.contains("Next").click();

      cy.get(
        ":nth-child(1) > :nth-child(2) > .relative > #main-editor > .no-user-select > .overflow-guard > .monaco-scrollable-element > .lines-content > .view-lines"
      ).type(challenge.updateChallengeCodeEvalDraft.snippet, {
        parseSpecialCharSequences: false,
      });

      cy.get(
        ":nth-child(2) > :nth-child(2) > .relative > #main-editor > .no-user-select > .overflow-guard > .monaco-scrollable-element > .lines-content > .view-lines"
      ).type(challenge.updateChallengeCodeEvalDraft.solution, {
        parseSpecialCharSequences: false,
      });
      cy.contains("Add test case").click();
      cy.get('input[name="testCases[0].description"]').type(
        challenge.updateChallengeCodeEvalDraft.DescriptionTC1
      );

      cy.get('textarea[name="testCases[0].input"]').type(
        challenge.updateChallengeCodeEvalDraft.inputTC1
      );

      cy.get('textarea[name="testCases[0].expectedOutput"]').type(
        challenge.updateChallengeCodeEvalDraft.ExpectValueTC1
      );

      cy.contains("Add test case").click();
      cy.get('input[name="testCases[1].description"]').type(
        challenge.updateChallengeCodeEvalDraft.DescriptionTC2
      );

      cy.get('textarea[name="testCases[1].input"]').type(
        challenge.updateChallengeCodeEvalDraft.inputTC2
      );

      cy.get('textarea[name="testCases[1].expectedOutput"]').type(
        challenge.updateChallengeCodeEvalDraft.ExpectValueTC2
      );
      cy.contains("Add test case").click();

      cy.get('input[name="testCases[2].description"]').type(
        challenge.updateChallengeCodeEvalDraft.DescriptionTC3
      );

      cy.get('textarea[name="testCases[2].input"]').type(
        challenge.updateChallengeCodeEvalDraft.inputTC3
      );

      cy.get('textarea[name="testCases[2].expectedOutput"]').type(
        challenge.updateChallengeCodeEvalDraft.ExpectValueTC3
      );
      cy.contains("Save & exit").click({ force: true });

      cy.location("pathname", { timeout: 60000 }).should(
        "eq",
        "/management/challenges"
      );
    });
  });
});
