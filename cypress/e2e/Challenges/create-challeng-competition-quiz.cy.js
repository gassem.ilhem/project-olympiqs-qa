import { generateRandomName } from "../../utils/helper";
describe("challenges", () => {
  let challenge;
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("challenge.json").then((data) => {
        challenge = data;
      });
    });
  });

  it("should successfully create a challenge competition quiz", () => {
    const { nameChallenge } = generateRandomName();
    cy.signInAsManager(manager.username, manager.password);
    cy.createSkillApi().then(function (firstResponse) {
      cy.navigateToChallengesPage();

      cy.contains("Create").click();

      cy.url().should("include", "/create");

      cy.contains("Next").click();
      cy.contains("Challenge details").parent().should("have.class", "active");
      cy.get('[data-cy="title"]').type(nameChallenge);
      cy.get('[data-cy="difficulty"]').select(
        challenge.challengeQuiz.Difficulty
      );
      cy.get('[data-cy="duration"]').type(challenge.challengeQuiz.Duration);
      cy.get('[data-cy="xpAtStake"]').type(challenge.challengeQuiz.XPs);
      cy.get('[data-cy="search-undefined"]').type(firstResponse.body.name);
      cy.contains(firstResponse.body.name).click();
      cy.contains("Next").click();
      cy.contains("Manage questions").parent().should("have.class", "active");

      //single options
      cy.contains("Add Question").click();
      cy.get("input[name='questions[0].hint']", { timeout: 60000 }).type("q1");
      cy.xpath(
        "/html/body/div[1]/div[1]/div[4]/div/div/div[2]/div/div/div/form/div[2]/div[1]/div[2]/div/div[6]/div/div/button"
      ).click();
      cy.xpath(
        "/html/body/div[1]/div[1]/div[4]/div/div/div[2]/div/div/div/form/div[2]/div[1]/div[2]/div/div[6]/div/div[1]/div/label/div/div/div/div/input",
        { timeout: 60000 }
      ).check({ force: true });
      cy.xpath(
        "/html/body/div[1]/div[1]/div[4]/div/div/div[2]/div/div/div/form/div[2]/div[1]/div[2]/div/div[6]/div/div[2]/button"
      ).click();

      //multiple options
      cy.contains("Add Question").click();
      cy.get("input[name='questions[1].hint']", { timeout: 60000 }).type("q2");
      cy.xpath(
        "/html/body/div[1]/div[1]/div[4]/div/div/div[2]/div/div/div/form/div[2]/div[2]/div[2]/div/div[5]/div/label[2]/div[1]/input",
        { timeout: 60000 }
      ).check({ force: true });
      cy.xpath(
        "/html/body/div[1]/div[1]/div[4]/div/div/div[2]/div/div/div/form/div[2]/div[2]/div[2]/div/div[6]/div/div/button"
      ).click();
      cy.xpath(
        "/html/body/div[1]/div[1]/div[4]/div/div/div[2]/div/div/div/form/div[2]/div[2]/div[2]/div/div[6]/div/div[1]/div/label/div/div/div/div/input",
        { timeout: 60000 }
      ).check({ force: true });
      cy.xpath(
        "/html/body/div[1]/div[1]/div[4]/div/div/div[2]/div/div/div/form/div[2]/div[2]/div[2]/div/div[6]/div/div[2]/button"
      ).click();
      cy.xpath(
        "/html/body/div[1]/div[1]/div[4]/div/div/div[2]/div/div/div/form/div[2]/div[2]/div[2]/div/div[6]/div/div[2]/div/label/div/div/div/div/input",
        { timeout: 60000 }
      ).check({ force: true });
      cy.xpath(
        "/html/body/div[1]/div[1]/div[4]/div/div/div[2]/div/div/div/form/div[2]/div[2]/div[2]/div/div[6]/div/div[3]/button"
      ).click();

      //textENtry

      cy.contains("Add Question").click();
      cy.get("input[name='questions[2].hint']", { timeout: 60000 }).type("q3");
      cy.xpath(
        "/html/body/div[1]/div[1]/div[4]/div/div/div[2]/div/div/div/form/div[2]/div[3]/div[2]/div/div[5]/div/label[3]/div[1]/input",
        { timeout: 60000 }
      ).check({ force: true });
      //cy.get("select[name='questions[0].options[0].type']").select("string")

      //multiple textEntry

      cy.contains("Add Question").click();
      cy.get("input[name='questions[3].hint']", { timeout: 60000 }).type("q4");
      cy.xpath(
        "/html/body/div[1]/div[1]/div[4]/div/div/div[2]/div/div/div/form/div[2]/div[4]/div[2]/div/div[5]/div/label[4]/div[1]/input",
        { timeout: 60000 }
      ).check({ force: true });
      cy.xpath(
        "/html/body/div[1]/div[1]/div[4]/div/div/div[2]/div/div/div/form/div[2]/div[4]/div[2]/div/div[6]/div/div/button"
      ).click();
      cy.xpath(
        "/html/body/div[1]/div[1]/div[4]/div/div/div[2]/div/div/div/form/div[2]/div[4]/div[2]/div/div[6]/div/div[3]/button"
      ).click();

      //Association

      cy.contains("Add Question").click();
      cy.get("input[name='questions[4].hint']", { timeout: 60000 }).type("q5");
      cy.xpath(
        "/html/body/div[1]/div[1]/div[4]/div/div/div[2]/div/div/div/form/div[2]/div[5]/div[2]/div/div[5]/div/label[5]/div[1]/input",
        { timeout: 60000 }
      ).check({ force: true });
      cy.xpath(
        "/html/body/div[1]/div[1]/div[4]/div/div/div[2]/div/div/div/form/div[2]/div[5]/div[2]/div/div[6]/div/div/button"
      ).click();
      cy.xpath(
        "/html/body/div[1]/div[1]/div[4]/div/div/div[2]/div/div/div/form/div[2]/div[5]/div[2]/div/div[6]/div/div[2]/button"
      ).click();
      cy.xpath(
        "/html/body/div[1]/div[1]/div[4]/div/div/div[2]/div/div/div/form/div[2]/div[5]/div[2]/div/div[6]/div/div[3]/button"
      ).click();

      //Free field
      cy.contains("Add Question").click();
      cy.get("input[name='questions[5].hint']", { timeout: 60000 }).type("q5");
      cy.xpath(
        "/html/body/div[1]/div[1]/div[4]/div/div/div[2]/div/div/div/form/div[2]/div[6]/div[2]/div/div[5]/div/label[6]/div[1]/input",
        { timeout: 60000 }
      ).check({ force: true });

      cy.contains("Next", { timeout: 600000000 }).click({ force: true });

      cy.contains("Submit").click();

      cy.location("pathname", { timeout: 60000 }).should(
        "eq",
        "/management/challenges"
      );
      cy.searchAndSubmit(nameChallenge);
      cy.contains(nameChallenge).should("be.visible");
    });
  });
});
