import { generateRandomName } from "../../utils/helper";
describe("challenges", () => {
  let challenge;
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
      cy.fixture("challenge.json").then((data) => {
        challenge = data;
      });
    });
  });
  it("should successfully create a challenge competition codeeval", () => {
    const { nameChallenge } = generateRandomName();
    cy.signInAsManager(manager.username, manager.password);
    cy.createSkillApi().then(function (firstResponse) {
      cy.navigateToChallengesPage();
      cy.contains("Create").click();
      cy.url().should("include", "/create");
      cy.contains("Code eval").click();
      cy.contains("Next").click();
      cy.get('[data-cy="title"]').type(nameChallenge);
      cy.get('[data-cy="difficulty"]').select(
        challenge.challengeCodeEvalCompetition.Difficulty
      );
      cy.get('[data-cy="duration"]').type(
        challenge.challengeCodeEvalCompetition.Duration
      );
      cy.get('[data-cy="xpAtStake"]').type(
        challenge.challengeCodeEvalCompetition.XPs
      );
      cy.get('[data-cy="search-undefined"]').type(firstResponse.body.name);
      cy.contains(firstResponse.body.name).click();
      cy.contains("Next").click();
      cy.get('[data-cy="search-undefined"]').type(
        challenge.challengeCodeEvalCompetition.languages
      );
      cy.contains(challenge.challengeCodeEvalCompetition.languages).click();
      cy.get(
        ":nth-child(1) > :nth-child(2) > .relative > #main-editor > .no-user-select > .overflow-guard > .monaco-scrollable-element > .lines-content > .view-lines"
      ).type(challenge.challengeCodeEvalCompetition.snippet, {
        parseSpecialCharSequences: false,
      });
      cy.get("select[id='solution.language']").select(
        challenge.challengeCodeEvalCompetition.solutionLanguage
      );
      cy.get(
        ":nth-child(2) > :nth-child(2) > .relative > #main-editor > .no-user-select > .overflow-guard > .monaco-scrollable-element > .lines-content > .view-lines"
      ).type(challenge.challengeCodeEvalCompetition.solution, {
        parseSpecialCharSequences: false,
      });

      cy.get('[data-cy="testCases[0].description"]').type(
        challenge.challengeCodeEvalCompetition.DescriptionTC1
      );

      cy.get('[data-cy="testCases[0].input"]').type(
        challenge.challengeCodeEvalCompetition.inputTC1
      );

      cy.get('[data-cy="testCases[0].expectedOutput"]').type(
        challenge.challengeCodeEvalCompetition.ExpectValueTC1
      );

      cy.contains("Add test case").click();
      cy.get('[data-cy="testCases[1].description"]').type(
        challenge.challengeCodeEvalCompetition.DescriptionTC2
      );

      cy.get('[data-cy="testCases[1].input"]').type(
        challenge.challengeCodeEvalCompetition.inputTC2
      );

      cy.get('[data-cy="testCases[1].expectedOutput"]').type(
        challenge.challengeCodeEvalCompetition.ExpectValueTC2
      );
      cy.contains("Add test case").click();

      cy.get('[data-cy="testCases[2].description"]').type(
        challenge.challengeCodeEvalCompetition.DescriptionTC3
      );

      cy.get('[data-cy="testCases[2].input"]').type(
        challenge.challengeCodeEvalCompetition.inputTC3
      );

      cy.get('[data-cy="testCases[2].expectedOutput"]').type(
        challenge.challengeCodeEvalCompetition.ExpectValueTC3
      );
      cy.contains("Next").click();

      cy.contains("Submit").click();

      cy.location("pathname", { timeout: 60000 }).should(
        "eq",
        "/management/challenges"
      );
      cy.searchAndSubmit(nameChallenge);
      cy.contains(nameChallenge).should("be.visible");
    });
  });
});
