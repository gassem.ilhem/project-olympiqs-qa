describe("challenges", () => {
  let bouton;
  let manager;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it("should successfully activate challenge disabled", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createQuizChallengeApiExemple().then((chall) => {
      cy.desactivateChallenge(chall.body.id).then((des) => {
        cy.navigateToChallengesPage();
        cy.searchAndSubmit(chall.body.title);
        cy.getByDataCy(chall.body.title + "-unlock")
          .first()
          .click({ force: true });
        cy.contains(bouton.submit).click();
        cy.videSearch();
        cy.searchAndSubmit(chall.body.title);
      });
    });
  });
});
