describe("challenges", () => {
  let challenge;
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
      cy.fixture("challenge.json").then((data) => {
        challenge = data;
      });
    });
  });
  it("should successfully update challenge codeeval published", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createCodeEvalChallengeExemple().then(function (firstResponse) {
      cy.navigateToChallengesPage();
      cy.searchAndSubmit(firstResponse.body.title);
      cy.wait(3000);
      cy.get("#pencil").click({ force: true });
      cy.location("pathname", { timeout: 60000 }).should("include", "/update");
      cy.contains("Competition").click();
      cy.contains("Next").click();
      cy.get("select#difficulty").select(
        challenge.updateChallengeCodeEvalPublished.Difficulty
      );
      cy.contains("Submit").click();
      cy.location("pathname", { timeout: 60000 }).should(
        "eq",
        "/management/challenges"
      );
      cy.searchAndSubmit(firstResponse.body.title);
      cy.contains(firstResponse.body.title).should("be.visible");
    });
  });
});
