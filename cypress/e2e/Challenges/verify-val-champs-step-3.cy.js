describe("challenges", () => {
  let challenge;
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
      cy.fixture("challenge.json").then((data) => {
        challenge = data;
      });
    });
  });
  it("should check empty field", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createSkillApi().then(function (firstResponse) {
      cy.navigateToChallengesPage();

      cy.contains("Create").click();

      cy.url().should("include", "/create");

      cy.contains("Code eval").click();

      cy.contains("Training").click();
      cy.contains("Next").click();
      cy.get("input#title").type(challenge.challengeCodeEvalTraining.Title);
      cy.get("select#difficulty").select(
        challenge.challengeCodeEvalTraining.Difficulty
      );
      cy.getByDataCy("duration").type(
        challenge.challengeCodeEvalTraining.Duration
      );
      cy.getByDataCy("xpAtStake").type(challenge.challengeCodeEvalTraining.XPs);
      cy.getByDataCy("repetitions").type(
        challenge.challengeCodeEvalTraining.numRepetitions
      );
      cy.get("input#search").type(firstResponse.body.name);
      cy.contains(firstResponse.body.name).click();
      cy.contains("Next").click();

      cy.contains("Next").click();
      cy.contains("This field is required").should("be.visible");
      cy.contains("at least 3 test cases are required.").should("be.visible");
    });
  });
});
