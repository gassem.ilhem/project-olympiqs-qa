import { generateRandomName } from "../../utils/helper";
describe("challenges", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it("should successfully activate challenge codeeval draft", () => {
    const { nameChallenge } = generateRandomName();
    cy.signInAsManager(manager.username, manager.password);
    cy.createDraftChallengeCodeEvalByApiWithoutBody(nameChallenge).then(
      (firstResponse) => {
        cy.navigateToChallengesPage();
        cy.getByDataCy(nameChallenge + "-unlock")
          .first()
          .click({ force: true });
        cy.contains("Submit").click();
        cy.videSearch();
        cy.searchAndSubmit(nameChallenge);
      }
    );
  });
});
