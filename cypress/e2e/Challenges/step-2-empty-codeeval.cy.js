describe("challenges", () => {
  let challenge;
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
      cy.fixture("challenge.json").then((data) => {
        challenge = data;
      });
    });
  });

  it("should check creat a challenge training codeeval step 2 empty", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.navigateToChallengesPage();

    cy.contains("Create").click();

    cy.url().should("include", "/create");

    cy.contains("Code eval").click();

    cy.contains("Training").click();
    cy.contains("Next").click();

    cy.contains("Next").click();
    cy.contains("Next").click();
    cy.get('[data-cy="search-undefined"]').type(
      challenge.challengeCodeEvalDraft.languages
    );
    cy.contains(challenge.challengeCodeEvalDraft.languages).click();
    cy.get(
      ":nth-child(1) > :nth-child(2) > .relative > #main-editor > .no-user-select > .overflow-guard > .monaco-scrollable-element > .lines-content > .view-lines"
    ).type(challenge.challengeCodeEvalDraft.snippet, {
      parseSpecialCharSequences: false,
    });

    cy.get("select[id='solution.language']").select(
      challenge.challengeCodeEvalDraft.solutionLanguage
    );
    cy.get(
      ":nth-child(2) > :nth-child(2) > .relative > #main-editor > .no-user-select > .overflow-guard > .monaco-scrollable-element > .lines-content > .view-lines"
    ).type(challenge.challengeCodeEvalDraft.solution, {
      parseSpecialCharSequences: false,
    });

    cy.get('[data-cy="testCases[0].description"]').type(
      challenge.challengeCodeEvalDraft.DescriptionTC1
    );

    cy.get('[data-cy="testCases[0].input"]').type(
      challenge.challengeCodeEvalDraft.inputTC1
    );

    cy.get('[data-cy="testCases[0].expectedOutput"]').type(
      challenge.challengeCodeEvalDraft.ExpectValueTC1
    );

    cy.contains("Add test case").click();
    cy.get('[data-cy="testCases[1].description"]').type(
      challenge.challengeCodeEvalDraft.DescriptionTC2
    );

    cy.get('[data-cy="testCases[1].input"]').type(
      challenge.challengeCodeEvalDraft.inputTC2
    );

    cy.get('[data-cy="testCases[1].expectedOutput"]').type(
      challenge.challengeCodeEvalDraft.ExpectValueTC2
    );
    cy.contains("Add test case").click();

    cy.get('[data-cy="testCases[2].description"]').type(
      challenge.challengeCodeEvalDraft.DescriptionTC3
    );

    cy.get('[data-cy="testCases[2].input"]').type(
      challenge.challengeCodeEvalDraft.inputTC3
    );

    cy.get('[data-cy="testCases[2].expectedOutput"]').type(
      challenge.challengeCodeEvalDraft.ExpectValueTC3
    );
    cy.contains("Next").click();
    cy.contains("confirm_dialog").should("be.visible");
    cy.contains("Confirm").click();

    cy.location("pathname", { timeout: 60000 }).should(
      "eq",
      "/management/challenges"
    );
    cy.searchAndSubmit(challenge.challengeCodeEvalDraft.Title);
    cy.contains(challenge.challengeCodeEvalDraft.Title).should("be.visible");
  });
});
