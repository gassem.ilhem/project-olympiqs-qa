describe("challenges", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it("should successfully delete challenge", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createCodeEvalChallengeExemple().then(function (firstResponse) {
      cy.navigateToChallengesPage();
      cy.searchAndSubmit(firstResponse.body.title);
      cy.getByDataCy(firstResponse.body.title + "-delete")
        .first()
        .click({
          force: true,
        });
      cy.get(".bg-primary").should("be.visible");
      cy.contains("Submit").click();
      cy.videSearch();
      cy.searchAndSubmit(firstResponse.body.title);
      cy.contains(firstResponse.body.title).should("not.exist", {
        timeout: 60000,
      });
    });
  });
});
