describe("challenges", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it("should successfully desactivate challenge codeeval", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createCodeEvalChallengeExemple().then(function (firstResponse) {
      cy.navigateToChallengesPage();
      cy.searchAndSubmit(firstResponse.body.title);
      cy.wait(3000);
      cy.getByDataCy(firstResponse.body.title + "-lock")
        .first()
        .click({ force: true });
      cy.contains("Submit").click();
      cy.videSearch();
      cy.searchAndSubmit(firstResponse.body.title);
      cy.contains(firstResponse.body.title).should("be.visible");
    });
  });
});
