describe("challenges", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it("should successfully see details challenge", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createCodeEvalChallengeExemple().then(function (firstResponse) {
      cy.navigateToChallengesPage();
      cy.searchAndSubmit(firstResponse.body.title);
      cy.getByDataCy(firstResponse.body.title + "-details")
        .first()
        .click({ force: true });
      cy.contains("Challenge detail").should("be.visible");
    });
  });
});
