describe("sessions", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it("should see details of session type competition", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createSessionCompetitionApi().then(function (sessionResponse) {
      cy.searchAndSubmit(sessionResponse.body.title, { timeout: 60000 });
      cy.get("a.css-14xk6wp")
        .first()
        .click({ force: true }, { timeout: 60000 });
      cy.url().should("include", "/detail");
    });
  });
});
