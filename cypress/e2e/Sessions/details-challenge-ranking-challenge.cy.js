describe("sessions", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it("should see ranking of challenge of session type competition", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createSessionCompetitionApi().then(function (sessionResponse) {
      cy.searchAndSubmit(sessionResponse.body.title, { timeout: 60000 });
      cy.get("a.css-14xk6wp")
        .first()
        .click({ force: true }, { timeout: 60000 });
      cy.url().should("include", "/detail");
      cy.wait(5000);
      cy.get("div:nth-child(1) > div > .flex .hidden svg").click({
        force: true,
      });
      cy.get(
        "div:nth-child(1) > div > .flex > .flex > .flex .mx-1:nth-child(3)"
      ).click({ force: true });
      cy.url().should("include", "/detail");
    });
  });
});
