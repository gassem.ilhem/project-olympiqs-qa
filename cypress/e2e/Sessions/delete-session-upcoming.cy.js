describe("sessions", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it("should delete a session upcoming", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createSessionCompetitionApi().then(function (sessionResponse) {
      cy.searchAndSubmit(sessionResponse.body.title);
      cy.wait(2000);
      cy.get("#delete").click({ force: true });
      cy.contains("Submit").click({ force: true });
      cy.videSearch();
      cy.searchAndSubmit(sessionResponse.body.title);
      cy.contains(sessionResponse.body.title).should("not.exist");
    });
  });
});
