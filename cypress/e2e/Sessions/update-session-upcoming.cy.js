describe("sessions", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it("should update  session upcoming type competition", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createSessionCompetitionApi().then(function (sessionResponse) {
      const sessionName = sessionResponse.body.title;
      cy.searchAndSubmit(sessionName, { timeout: 60000 });
      cy.get("#pencil").first().click({ force: true }, { timeout: 60000 });
      cy.url().should("include", "/update");
      cy.get("#title")
        .clear()
        .type("edited " + sessionName);
      cy.contains("Submit").click();
      cy.contains("edited " + sessionName).should("exist");
    });
  });
});
