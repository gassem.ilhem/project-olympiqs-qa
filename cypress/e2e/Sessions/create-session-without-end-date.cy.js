import { generateRandomName } from "../../utils/helper";
describe("sessions", () => {
  let manager;
  let bouton;
  let message;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
        message = data.errorMessages;
      });
    });
  });
  it("should creat a session type competition without end date", () => {
    const { nameSession, description } = generateRandomName();
    cy.signInAsManager(manager.username, manager.password);
    cy.createCommunityApi().then(function (communityResponse) {
      cy.contains("Create Session").click();

      cy.url().should("include", "/create");

      cy.contains("Next").click();

      cy.contains("Next").click();
      cy.contains("Session details").parent().should("have.class", "active");
      cy.get('[data-cy="title"]').type(nameSession);
      cy.get('[data-cy="search-undefined"]').type(communityResponse.body.name);
      cy.contains(communityResponse.body.name).click();
      cy.get("input#endedAt").clear();
      cy.get('[data-cy="description"]').type(description);
      cy.contains("Next").click();
      cy.contains(message.invalidDate).should("be.visible");
      cy.url().should("include", "/create");
    });
  });
});
