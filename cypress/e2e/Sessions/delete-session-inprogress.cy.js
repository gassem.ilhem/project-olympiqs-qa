describe("sessions", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it("should creat a session type competition", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createSessionCompetitionInProgressApi().then(function (sessionResponse) {
      cy.searchAndSubmit(sessionResponse.body.title, { timeout: 60000 });
      cy.get("svg.text-danger g#delete path#Tracé_662")
        .first()
        .click({ force: true }, { timeout: 60000 });
      cy.get(".bg-primary", { timeout: 60000 }).should("be.visible");

      cy.contains("Submit", { timeout: 60000 }).click();
      cy.videSearch();
      cy.searchAndSubmit(sessionResponse.body.title);
      cy.contains(sessionResponse.body.title).should("be.visible");
    });
  });
});
