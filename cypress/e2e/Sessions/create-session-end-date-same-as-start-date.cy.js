import { generateRandomName } from "../../utils/helper";
describe("sessions", () => {
  let manager;
  let bouton;
  let message;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
        message = data.errorMessages;
      });
    });
  });
  it("should creat a session type competition end date the same as the start date", () => {
    const { nameSession, description } = generateRandomName();
    cy.signInAsManager(manager.username, manager.password);
    cy.createCommunityApi().then(function (communityResponse) {
      cy.createQuizChallengeApiExemple().then(function (challengeResponse) {
        cy.generateSessionDate().then((sessionDate) => {
          cy.contains("Create Session").click();

          cy.url().should("include", "/create");

          cy.contains("Next").click();
          cy.contains("Session details")
            .parent()
            .should("have.class", "active");
          cy.get('[data-cy="title"]').type(nameSession);
          cy.get('[data-cy="search-undefined"]').type(
            communityResponse.body.name
          );
          cy.contains(communityResponse.body.name).click();
          cy.get("input#startedAt").type(sessionDate);
          cy.get("input#endedAt").type(sessionDate);
          cy.get('[data-cy="description"]').type(description);

          cy.contains("Next").click();

          cy.contains("Manage challenges")
            .parent()
            .should("have.class", "active");
          cy.contains("Add Challenge").click();
          cy.get('[data-cy="search-undefined"]').type(
            challengeResponse.body.title,
            { force: true }
          );

          cy.contains(challengeResponse.body.title).click();
          cy.contains(message.ChallStartDateAfterstartDateSession).should(
            "be.visible"
          );
        });
      });
    });
  });
});
