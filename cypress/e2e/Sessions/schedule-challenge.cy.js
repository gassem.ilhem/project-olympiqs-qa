import { generateSessionDatePlusOne } from "../../utils/helper";
describe("sessions", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it("should schecule challenge for session type competition", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createSessionCompetitionApi().then(function (sessionResponse) {
      cy.createQuizChallengeApiExemple().then(function (challengeResponse) {
        cy.reload();
        cy.wait(5000);
        cy.get(
          "#tabs-1-tab-0 > table > tbody > tr:nth-child(1) > td:nth-child(6) > div > a:nth-child(2)",
          { timeout: 60000 }
        ).click({ force: true });
        cy.get("input#challenge").type(challengeResponse.body.title);
        cy.contains(challengeResponse.body.title).click({ force: true });
        cy.get("input#startedAt").clear();
        cy.get("input#startedAt").type(generateSessionDatePlusOne(), {
          force: true,
        });
        cy.contains("Submit").click({ force: true });
        cy.url().should("include", "/detail");
      });
    });
  });
});
