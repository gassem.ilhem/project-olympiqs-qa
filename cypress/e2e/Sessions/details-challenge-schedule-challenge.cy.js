import { generateSessionDatePlusOne } from "../../utils/helper";
describe("sessions", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it("should see details of session type competition", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createSessionCompetitionApi().then(function (sessionResponse) {
      cy.createQuizChallengeApiExemple().then(function (challengeResponse) {
        cy.reload();
        cy.searchAndSubmit(sessionResponse.body.title, { timeout: 60000 });

        cy.get("a.css-14xk6wp")
          .first()
          .click({ force: true }, { timeout: 60000 });
        cy.url().should("include", "/detail");
        cy.contains("Schedule challenge").click({ force: true });
        cy.url().should("include", "/schedule");

        cy.get("input#challenge").type(challengeResponse.body.title);
        cy.contains(challengeResponse.body.title).click({ force: true });
        cy.get("input#startedAt").clear();
        cy.get("input#startedAt").type(generateSessionDatePlusOne(), {
          force: true,
        });
        cy.contains("Submit").click({ force: true });
        cy.url().should("include", "/detail");
      });
    });
  });
});
