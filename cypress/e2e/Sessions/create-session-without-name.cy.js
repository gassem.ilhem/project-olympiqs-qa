import { generateRandomName } from "../../utils/helper";
describe("sessions", () => {
  let manager;
  let bouton;
  let message;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
        message = data.errorMessages;
      });
    });
  });
  it("should creat a session type competition without name", () => {
    const { description } = generateRandomName();
    cy.signInAsManager(manager.username, manager.password);
    cy.createCommunityApi().then(function (communityResponse) {
      cy.contains("Create Session").click();

      cy.url().should("include", "/create");

      cy.contains("Next").click();

      cy.contains("Next").click();
      cy.contains("Session details").parent().should("have.class", "active");

      cy.get('[data-cy="search-undefined"]').type(communityResponse.body.name);
      cy.contains(communityResponse.body.name).click();
      cy.get('[data-cy="description"]').type(description);

      cy.contains("Next").click();
      cy.contains(message.fieldRequired).should("be.visible");
      cy.url().should("include", "/create");
    });
  });
});
