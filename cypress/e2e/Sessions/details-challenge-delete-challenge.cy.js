describe("sessions", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it("should delete challenge of session type competition", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createSessionCompetitionApi().then(function (sessionResponse) {
      cy.reload();
      cy.get("a.css-14xk6wp", { timeout: 60000 })
        .first()
        .click({ force: true });
      cy.url().should("include", "/detail");
      cy.wait(5000);
      cy.get("div:nth-child(1) > div > .flex .hidden svg").click({
        force: true,
      });
      cy.get(
        "div:nth-child(1) > div > .flex > .flex > .flex .mx-1:nth-child(1)"
      ).click({ force: true });
      cy.contains("Submit").click();
    });
  });
});
