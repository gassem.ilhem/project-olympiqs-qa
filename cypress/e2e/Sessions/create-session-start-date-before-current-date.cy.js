import { generateRandomName } from "../../utils/helper";
describe("sessions", () => {
  let manager;
  let bouton;
  let message;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
        message = data.errorMessages;
      });
    });
  });
  it("should creat a session type competition start date before current date", () => {
    const { nameSession, description } = generateRandomName();
    cy.signInAsManager(manager.username, manager.password);
    cy.createCommunityApi().then(function (communityResponse) {
      cy.generateBeforeCurrentDate().then((sessionDate) => {
        cy.generateSessionEndDate().then((sessionEndDate) => {
          cy.contains("Create Session").click();

          cy.url().should("include", "/create");

          cy.contains("Next").click();
          cy.contains("Session details")
            .parent()
            .should("have.class", "active");
          cy.get('[data-cy="title"]').type(nameSession);
          cy.get('[data-cy="search-undefined"]').type(
            communityResponse.body.name
          );
          cy.contains(communityResponse.body.name).click();
          cy.get("input#startedAt").type(sessionDate);
          cy.get("input#endedAt").type(sessionEndDate);
          cy.get('[data-cy="description"]').type(description);

          cy.contains("Next").click();
          cy.contains(message.startDateBeforeCurrentDate).should("be.visible");
          cy.url().should("include", "/create");
        });
      });
    });
  });
});
