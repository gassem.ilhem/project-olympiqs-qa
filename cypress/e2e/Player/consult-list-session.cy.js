import { generatePassword } from "../../utils/helper";
describe("player", () => {
  it("should successfully consult list sessions", () => {
    const password = generatePassword();
    cy.loginAPI("manager", "manager");
    cy.createCommunityApi().then(function (responseCommunity) {
      cy.createPlayerWithCommunityApi(responseCommunity.body.id).then(
        (player) => {
          cy.getUserApi(player.body.data[0].email).then((session) => {
            cy.activatePlayerPendingPassword(
              session.body.data[0].id,
              password,
              password
            ).then((sessionDate) => {
              cy.createSessionCompetitionWithCommunityApi(
                responseCommunity.body
              ).then((sessionBdoy) => {
                cy.wait(9000);
                cy.clearToken();
                cy.wait(3000);
                cy.signIn(player.body.data[0].email, password);
                cy.wait(3000);
                cy.location("pathname").should("eq", "/competitions");
                cy.getCookie("__access_token__").should("exist");
                cy.contains("All sessions").should("be.visible");
                cy.contains(`${sessionBdoy.body.title}`).should("be.visible");
              });
            });
          });
        }
      );
    });
  });
});
