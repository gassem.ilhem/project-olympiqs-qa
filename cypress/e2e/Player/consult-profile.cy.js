import {
  generateRandomName,
  generateRandomEmail,
  generatePassword,
} from "../../utils/helper";
describe("player", () => {
  it("should successfully consult his profile", () => {
    const { firstName, lastName } = generateRandomName();
    const email = generateRandomEmail("player");
    const password = generatePassword();
    cy.loginAPI("manager", "manager");
    cy.createPlayerApi(firstName, lastName, email).then((player) => {
      cy.getUserApi(player.body.data[0].email).then((play) => {
        cy.activatePlayerPendingPassword(
          play.body.data[0].id,
          password,
          password
        ).then((playerActivat) => {
          cy.wait(9000);
          cy.clearToken();
          cy.wait(3000);
          cy.signIn(player.body.data[0].email, password);
          cy.wait(3000);
          cy.location("pathname").should("eq", "/competitions");
          cy.navigateToProfilePage();
        });
      });
    });
  });
});
