import {
  generateRandomName,
  generateRandomEmail,
  generatePassword,
} from "../../utils/helper";
describe("player", () => {
  let profile;

  beforeEach(() => {
    cy.fixture("profile.json").then((data) => {
      profile = data;
    });
  });

  it("should successfully edit his profile", () => {
    const { firstName, lastName } = generateRandomName();
    const email = generateRandomEmail("player");
    const password = generatePassword();
    cy.loginAPI("manager", "manager");
    cy.createPlayerApi(firstName, lastName, email).then((player) => {
      cy.getUserApi(player.body.data[0].email).then((play) => {
        cy.activatePlayerPendingPassword(
          play.body.data[0].id,
          password,
          password
        ).then((playerActivat) => {
          cy.wait(9000);
          cy.clearToken();
          cy.wait(3000);
          cy.signIn(player.body.data[0].email, password);
          cy.wait(3000);
          cy.location("pathname").should("eq", "/competitions");
          cy.navigateToProfilePage();
          cy.contains("Edit profile").click();
          cy.location("pathname", { timeout: 60000 }).should(
            "include",
            "/edit"
          );
          cy.get("input#firstName").type(profile.editProfile.firstName);
          cy.get("input#lastName").type(profile.editProfile.lastName);
          cy.contains("Validate").click();

          cy.location("pathname", { timeout: 60000 }).should("eq", "/profile");
          cy.contains(profile.editProfile.firstName).should("be.visible");
        });
      });
    });
  });
});
