import { generateRandomName, generateRandomEmail } from "../../utils/helper";
describe("player", () => {
  it("should successfully consult communities", () => {
    const { firstName, lastName } = generateRandomName();
    const email = generateRandomEmail("player");
    cy.loginAPI("manager", "manager");
    cy.createPlayerApi(firstName, lastName, email).then((player) => {
      cy.getUserApi(player.body.data[0].email).then((play) => {
        cy.activatePlayerPendingPassword(
          play.body.data[0].id,
          "Ilhem$98",
          "Ilhem$98"
        ).then((playerActivat) => {
          cy.wait(9000);
          cy.clearToken();
          cy.wait(3000);
          cy.signIn(player.body.data[0].email, "Ilhem$98");
          cy.wait(3000);
          cy.location("pathname").should("eq", "/competitions");
          cy.navigateToCommunityPage();
        });
      });
    });
  });
});
