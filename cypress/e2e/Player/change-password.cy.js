import { generateRandomName, generateRandomEmail } from "../../utils/helper";
describe("player", () => {
  let profile;

  beforeEach(() => {
    cy.fixture("profile.json").then((data) => {
      profile = data;
    });
  });

  it("should successfully change his password", () => {
    const { firstName, lastName } = generateRandomName();
    const email = generateRandomEmail("player");
    cy.loginAPI("manager", "manager");
    cy.createPlayerApi(firstName, lastName, email).then((player) => {
      cy.getUserApi(player.body.data[0].email).then((play) => {
        cy.activatePlayerPendingPassword(
          play.body.data[0].id,
          "Ilhem$98",
          "Ilhem$98"
        ).then((playerActivat) => {
          cy.wait(9000);
          cy.clearToken();
          cy.wait(3000);
          cy.signIn(player.body.data[0].email, "Ilhem$98");
          cy.wait(3000);
          cy.location("pathname").should("eq", "/competitions");
          cy.navigateToProfilePage();
          cy.contains("Edit profile").click();
          cy.contains("Change password").click();
          cy.contains("Changing password").should("be.visible");
          cy.get("input#plainOldPassword").type(
            profile.changePassword.actualPassword
          );
          cy.get("input#newPlainPassword").type(
            profile.changePassword.newPassword
          );
          cy.get("input#confirmPlainPassword").type(
            profile.changePassword.confirmPassword
          );
          cy.contains("Submit").click();
          cy.clearToken();
          cy.signIn(
            player.body.data[0].email,
            profile.changePassword.newPassword
          );
          cy.location("pathname").should("eq", "/competitions");
        });
      });
    });
  });
});
