import { generatePassword } from "../../utils/helper";
describe("player", () => {
  it("should successfully win a challenge quiz", () => {
    const password = generatePassword();
    console.log(password);
    cy.loginAPI("manager", "manager");
    cy.createCommunityApi().then(function (responseCommunity) {
      cy.createPlayerWithCommunityApi(responseCommunity.body.id).then(
        (player) => {
          cy.getUserApi(player.body.data[0].email).then((session) => {
            cy.activatePlayerPendingPassword(
              session.body.data[0].id,
              password,
              password
            ).then((sessionDate) => {
              cy.createQuizChallengeApiExemple().then(function (
                responseChallenge
              ) {
                cy.createSessionCompetitionWithChallengeApi(
                  responseCommunity.body,
                  responseChallenge.body.id,
                  responseChallenge.body.title
                ).then((sessionBdoy) => {
                  cy.wait(9000);
                  cy.clearToken();
                  cy.signIn(player.body.data[0].email, password);
                  cy.contains("All sessions").should("be.visible");
                  cy.contains(`${sessionBdoy.body.title}`).should("be.visible");
                  cy.getCookie("__access_token__").should("exist");
                  cy.contains("All sessions").should("be.visible");
                  cy.get(".css-u56vkx > svg > path").click();

                  cy.location("pathname", { timeout: 60000 }).should(
                    "include",
                    "/detail"
                  );
                  const title = responseChallenge.body.title + "-play";
                  cy.get('a[data-cy="' + title + '"]:eq(1)', {
                    timeout: 10000,
                  }).click({
                    force: true,
                  });
                  cy.get('[data-cy="take-challenge-btn"]', {
                    timeout: 10000,
                  }).click({
                    force: true,
                  });
                  cy.fixture("Api/challenge-quiz-api.json").then(function (
                    quiz
                  ) {
                    const lengthQuestion = quiz.questions.length - 1;
                    quiz.questions.forEach((questions, index) => {
                      if (questions.mode == "single-entry") {
                        cy.singleEntry(
                          questions.options.find((op) => op.correct).content
                        );
                      } else if (questions.mode == "multi-entries") {
                        const correctAnswers = questions.options.filter(
                          (option) => {
                            return option.correct == true;
                          }
                        );
                        cy.log(correctAnswers);
                        cy.multiEntries(correctAnswers);
                      } else if (questions.mode == "text-entry") {
                        const answers = questions.options.map((option) => {
                          return option.content;
                        });
                        cy.textEntry(answers);
                      } else {
                        const answers = questions.options.map((option) => {
                          return option.content;
                        });
                        cy.multiTextEntries(answers);
                      }
                      cy.confirmAnswers(index, lengthQuestion);
                    });
                  });
                  cy.contains("Congrats").should("be.visible");
                  cy.contains("Close").click({ force: true });
                  cy.contains("100 %").should("exist");
                });
              });
            });
          });
        }
      );
    });
  });
});
