import { generatePassword } from "../../utils/helper";
describe("player", () => {
  it("should successfully consult list sessions", () => {
    const password = generatePassword();
    cy.loginAPI("manager", "manager");
    cy.createCommunityApi().then(function (responseCommunity) {
      cy.createPlayerWithCommunityApi(responseCommunity.body.id).then(
        (player) => {
          cy.getUserApi(player.body.data[0].email).then((session) => {
            cy.activatePlayerPendingPassword(
              session.body.data[0].id,
              password,
              password
            ).then((sessionDate) => {
              console.log("manger :", session);

              cy.createQuizChallengeApiExemple().then(function (
                responseChallenge
              ) {
                cy.createSessionCompetitionWithChallengeApi(
                  responseCommunity.body,
                  responseChallenge.body.id,
                  responseChallenge.body.title
                ).then((sessionBdoy) => {
                  const session = sessionBdoy;
                  console.log(session);

                  cy.wait(9000);
                  cy.clearToken();
                  cy.wait(3000);
                  cy.signIn(player.body.data[0].email, password);
                  cy.wait(3000);
                  cy.location("pathname").should("eq", "/competitions");
                  cy.getCookie("__access_token__").should("exist");

                  cy.get(".css-u56vkx > svg > path").click();
                  cy.location("pathname", { timeout: 60000 }).should(
                    "include",
                    "/detail"
                  );
                  cy.contains("Achievements & Ranking").click();
                  cy.get(
                    ".mx-1 > :nth-child(1) > .bg-primary > :nth-child(1) > .font-semibold"
                  ).should("be.visible");
                });
              });
            });
          });
        }
      );
    });
  });
});
