import { generatePassword } from "../../utils/helper";
describe("player", () => {
  it("should lost a challenge quiz", () => {
    const password = generatePassword();
    cy.loginAPI("manager", "manager");
    cy.createCommunityApi().then(function (responseCommunity) {
      cy.createPlayerWithCommunityApi(responseCommunity.body.id).then(
        (player) => {
          cy.getUserApi(player.body.data[0].email).then((session) => {
            cy.activatePlayerPendingPassword(
              session.body.data[0].id,
              password,
              password
            ).then((sessionDate) => {
              cy.createQuizChallengeApiLost().then(function (
                responseChallenge
              ) {
                cy.createSessionCompetitionWithChallengeApi(
                  responseCommunity.body,
                  responseChallenge.body.id,
                  responseChallenge.body.title
                ).then((sessionBdoy) => {
                  cy.wait(9000);
                  cy.clearToken();
                  cy.signIn(player.body.data[0].email, password);
                  cy.contains("All sessions").should("be.visible");
                  cy.contains(`${sessionBdoy.body.title}`).should("be.visible");
                  cy.getCookie("__access_token__").should("exist");
                  cy.contains("All sessions").should("be.visible");
                  cy.get(".css-u56vkx > svg > path").click();

                  cy.location("pathname", { timeout: 60000 }).should(
                    "include",
                    "/detail"
                  );
                  const title = responseChallenge.body.title + "-play";
                  cy.get('a[data-cy="' + title + '"]:eq(1)', {
                    timeout: 10000,
                  }).click({
                    force: true,
                  });
                  cy.get('[data-cy="take-challenge-btn"]', {
                    timeout: 10000,
                  }).click({
                    force: true,
                  });
                  cy.fixture("Api/challenge-quiz-lost-api.json").then(function (
                    quiz
                  ) {
                    const lengthQuestion = quiz.questions.length - 1;
                    quiz.questions.forEach((questions, index) => {
                      cy.singleEntry(
                        questions.options.find((op) => op.correct).content
                      );

                      cy.confirmAnswers(index, lengthQuestion);
                    });
                  });
                  cy.contains(
                    "You didn't succeed in this challenge, better luck next time!"
                  ).should("be.visible");
                  cy.contains("Close").click({ force: true });
                });
              });
            });
          });
        }
      );
    });
  });
});
