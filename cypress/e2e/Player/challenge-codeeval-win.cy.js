import { generatePassword } from "../../utils/helper";
describe("player", () => {
  it("should successfully win a challenge codeeval", () => {
    const password = generatePassword();
    cy.loginAPI("manager", "manager");
    cy.createCommunityApi().then(function (responseCommunity) {
      cy.createPlayerWithCommunityApi(responseCommunity.body.id).then(
        (player) => {
          cy.getUserApi(player.body.data[0].email).then((session) => {
            cy.activatePlayerPendingPassword(
              session.body.data[0].id,
              password,
              password
            ).then((sessionDate) => {
              cy.createCodeEvalChallengeExemple().then(function (
                responseChallenge
              ) {
                cy.createSessionCompetitionWithChallengeApi(
                  responseCommunity.body,
                  responseChallenge.body.id,
                  responseChallenge.body.title
                ).then((sessionBdoy) => {
                  cy.wait(9000);
                  cy.clearToken();
                  cy.signIn(player.body.data[0].email, password);
                  cy.contains("All sessions").should("be.visible");
                  cy.contains(`${sessionBdoy.body.title}`).should("be.visible");
                  cy.getCookie("__access_token__").should("exist");
                  cy.contains("All sessions").should("be.visible");
                  cy.get(".css-u56vkx > svg > path").click();

                  cy.location("pathname", { timeout: 60000 }).should(
                    "include",
                    "/detail"
                  );

                  const title = responseChallenge.body.title + "-play";

                  cy.get('a[data-cy="' + title + '"]:eq(1)', {
                    timeout: 10000,
                  }).click({
                    force: true,
                  });
                  cy.contains("Take the challenge", { timeout: 10000 }).click({
                    force: true,
                  });
                  cy.fixture("Api/challenge-codeeval-api.json").then(function (
                    codeeval
                  ) {
                    cy.wait(10000);
                    cy.get("textarea").type("{ctrl}{a}{del}", {
                      force: true,
                    });
                    cy.get("textarea").type(codeeval.solution.sourceCode, {
                      force: true,
                      parseSpecialCharSequences: false,
                      delay: 20,
                    });
                  });
                  cy.contains("Submit").click();
                  cy.get('div[role="dialog"]').should("be.visible");
                  cy.get("div[data-reach-dialog-overlay]").within(() => {
                    cy.contains("Would you like to submit this challenge ?");
                    cy.contains("Submit").click({ force: true });
                    cy.contains("Close").click({ force: true });
                    cy.wait(5000);
                    cy.get('a[data-cy="' + title + '"]:eq(1)', {
                      timeout: 10000,
                    }).click({
                      force: true,
                    });
                    cy.contains("Congrats").should("be.visible");
                  });
                });
              });
            });
          });
        }
      );
    });
  });
});
