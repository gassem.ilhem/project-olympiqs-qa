describe("skills", () => {
  let message;
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
        message = data.errorMessages;
      });
    });
  });
  it("should successfully delete a skill", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createQuizChallengeApiExemple().then(function (responseSkill) {
      const skillNames = responseSkill.body.skills.map((skill) => skill.name);
      cy.navigateToSkillsPage();

      cy.searchAndSubmit(skillNames);

      cy.get("#delete").click({ force: true });
      cy.get(".bg-primary").should("be.visible");
      cy.contains("Confirm").click();
      cy.videSearch();
      cy.searchAndSubmit(skillNames);
      cy.contains(skillname).should("be.visible");
    });
  });
});
