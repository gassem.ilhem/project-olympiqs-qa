describe("skills", () => {
  let manager;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
    });
  });
  it("should successfully delete a skill", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createSkillApi().then(function (responseSkill) {
      cy.navigateToSkillsPage();

      cy.searchAndSubmit(responseSkill.body.name);

      cy.get("#delete").click({ force: true });
      cy.get(".bg-primary").should("be.visible");
      cy.contains("Confirm").click();
      cy.videSearch();
      cy.searchAndSubmit(responseSkill.body.name);
      cy.contains(responseSkill.body.name).should("not.exist");
    });
  });
});
