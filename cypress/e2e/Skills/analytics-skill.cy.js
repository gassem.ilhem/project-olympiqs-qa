describe("skills", () => {
  let manager;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
    });
  });
  it("see analytics skill ", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.navigateToSkillsPage();

    cy.get(
      "#root > div.mx-auto.flex > div.w-full > div > section > div.flex.items-center.justify-between > header > button > a > svg"
    ).click();

    cy.url().should("include", "/analytics");
  });
});
