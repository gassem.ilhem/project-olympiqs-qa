import { generateRandomName } from "../../utils/helper";
describe("skills", () => {
  let manager;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
    });
  });

  it("should search a non exsit skill", () => {
    const { nameSkill } = generateRandomName();
    cy.signInAsManager(manager.username, manager.password);
    cy.navigateToSkillsPage();
    cy.get('input[name="term"]').type(`${nameSkill}{enter}`);
    cy.contains(nameSkill).should("not.exist");
  });
});
