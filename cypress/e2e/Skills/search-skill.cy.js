describe("skills", () => {
  let manager;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
    });
  });
  it("should search an exsit skill", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createSkillApi().then(function (responseSkill) {
      cy.navigateToSkillsPage();
      cy.get('input[name="term"]').type(`${responseSkill.body.name}{enter}`);
      cy.contains(responseSkill.body.name).should("be.visible");
    });
  });
});
