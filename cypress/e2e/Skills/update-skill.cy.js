describe("skills", () => {
  let manager;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
    });
  });
  it("should successfully update a skill", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createSkillApi().then(function (responseSkill) {
      cy.navigateToSkillsPage();

      cy.searchAndSubmit(responseSkill.body.name);

      cy.get("#pencil").click({ force: true });
      cy.get('[data-cy="name"]').should("have.value", responseSkill.body.name);
      cy.get('[data-cy="name"]')
        .clear()
        .type("edited " + responseSkill.body.name);
      cy.contains("Submit").click();
      cy.contains("edited " + responseSkill.body.name).should("exist");
    });
  });
});
