import { generateRandomName, colorCode } from "../../utils/helper";
describe("skills", () => {
  let message;
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
        message = data.errorMessages;
      });
    });
  });
  it("should successfully create a skill", () => {
    const { nameSkill } = generateRandomName();
    const color = `#ff${colorCode}`;
    cy.signInAsManager(manager.username, manager.password);
    cy.navigateToSkillsPage();
    cy.contains("Create").click();
    cy.url().should("include", "/create");
    cy.get('[data-cy="name"]').type(nameSkill);
    cy.get("#color").invoke("val", color).trigger("change");
    cy.get("#color").should("have.value", color);
    cy.contains("Submit").click();
    cy.location("pathname", { timeout: 60000 }).should(
      "eq",
      "/management/skills"
    );
    cy.searchAndSubmit(nameSkill);
    cy.contains(nameSkill).should("be.visible");
  });
});
