import { generateRandomName, colorCode } from "../../utils/helper";
describe("skills", () => {
  let message;
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
        message = data.errorMessages;
      });
    });
  });
  it("create a skill without name", () => {
    const color = `#ff${colorCode}`;
    cy.signInAsManager(manager.username, manager.password);
    cy.navigateToSkillsPage();

    cy.contains("Create").click();

    cy.url().should("include", "/create");

    cy.get('[data-cy="color"]').invoke("val", color).trigger("change");

    cy.get('[data-cy="color"]').should("have.value", color);

    cy.contains("Submit").click();

    cy.contains(message.fieldRequired).should("be.visible");
    cy.url().should("include", "/create");
  });
});
