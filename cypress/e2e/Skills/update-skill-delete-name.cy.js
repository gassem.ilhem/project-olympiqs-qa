describe("skills", () => {
  let message;
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
        message = data.errorMessages;
      });
    });
  });
  it("should update a skill delete name", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createSkillApi().then(function (responseSkill) {
      cy.navigateToSkillsPage();

      cy.searchAndSubmit(responseSkill.body.name);

      cy.get("#pencil").click({ force: true });
      cy.get("input#name").clear();

      cy.contains("Submit").click();
      cy.contains(message.fieldRequired).should("be.visible");
    });
  });
});
