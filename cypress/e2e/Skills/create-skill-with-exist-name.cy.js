describe("skills", () => {
  let message;
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
        message = data.errorMessages;
      });
    });
  });
  it("create a skill with exist name", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createSkillApi().then(function (responseSkill) {
      cy.navigateToSkillsPage();

      cy.contains("Create").click();

      cy.url().should("include", "/create");
      cy.get('[data-cy="name"]').type(responseSkill.body.name);

      cy.contains("Submit").click();

      cy.contains(message.skillExist).should("be.visible");
      cy.url().should("include", "/create");
    });
  });
});
