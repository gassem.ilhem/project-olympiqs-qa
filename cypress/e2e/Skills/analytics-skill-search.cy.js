describe("skills", () => {
  let manager;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
    });
  });
  it("should search a skill in analytics skill ", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createSkillApi().then(function (responseSkill) {
      cy.navigateToSkillsPage();
      cy.get(
        "#root > div.mx-auto.flex > div.w-full > div > section > div.flex.items-center.justify-between > header > button > a > svg"
      ).click();
      cy.url().should("include", "/analytics");
      cy.searchAndSubmit(responseSkill.body.name);
    });
  });
});
