describe("logs", () => {
  let manager;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
    });
  });
  it("should successfully delete a community", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.navigateToLogsPage();
  });
});
