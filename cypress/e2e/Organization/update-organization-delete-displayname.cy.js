describe("organization", () => {
  let msgError;
  beforeEach(() => {
    cy.signInAsAdmin();

    cy.fixture("msg-error.json").then((data) => {
      msgError = data;
    });
  });

  it("should update a organization delte displayname", () => {
    cy.get("#pencil").click({ force: true });
    cy.get("input#displayName").clear();
    cy.contains("Submit").click();
    cy.contains(msgError.fieldRequired.msg).should("be.visible");
  });
});
