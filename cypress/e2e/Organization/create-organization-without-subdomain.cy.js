describe("organization", () => {
  const generateUniqueOrganizationtyName = () => {
    return `organization-${Date.now()}-${Cypress._.random(1000, 9999)}`; // Generate a unique skill name
  };
  let organization;
  let msgError;
  const organizationName = generateUniqueOrganizationtyName();
  beforeEach(() => {
    cy.signInAsAdmin();
    cy.fixture("organization.json").then((data) => {
      organization = data;
    });
    cy.fixture("msg-error.json").then((data) => {
      msgError = data;
    });
  });

  it("should successfully create a organization without subdomain", () => {
    cy.contains("Create Organization").click();

    cy.url().should("include", "/create");
    cy.get("input#name").type(organizationName);
    cy.get("input#displayName").type(
      organization.organizationCreate.displayName
    );

    cy.get(
      'input[type="checkbox"][name="settings.challengeSettings.enabledChallenges"][value="quiz"]'
    ).check();

    cy.get(
      'input[type="checkbox"][name="settings.challengeSettings.enabledChallenges"][value="codeeval"]'
    ).check();

    cy.get(
      'input[type="checkbox"][name="settings.challengeSettings.enabledChallenges"][value="projecteval"]'
    ).check();

    cy.get(
      'div.relative.inline-block.w-10.mr-2.align-middle.select-none.transition.duration-200.ease-in input[type="checkbox"][id="settings.uiSettings.profileSocialLinks"]'
    ).check();
    cy.get(
      'div.relative.inline-block.w-10.mr-2.align-middle.select-none.transition.duration-200.ease-in input[type="checkbox"][id="settings.uiSettings.resumeAccessible"]'
    ).check();

    cy.contains("Submit").click();
    cy.contains(msgError.fieldRequired.msg).should("be.visible");
  });
});
