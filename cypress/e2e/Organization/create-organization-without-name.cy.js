describe("organization", () => {
  let organization;
  let msgError;

  beforeEach(() => {
    cy.signInAsAdmin();
    cy.fixture("organization.json").then((data) => {
      organization = data;
    });
    cy.fixture("msg-error.json").then((data) => {
      msgError = data;
    });
  });

  it("should successfully create a organization without name", () => {
    cy.contains("Create Organization").click();

    cy.url().should("include", "/create");

    cy.get("input#displayName").type(
      organization.organizationCreate.displayName
    );
    cy.get("input#subDomain").type(organization.organizationCreate.subDomain);
    cy.get(
      'input[type="checkbox"][name="settings.challengeSettings.enabledChallenges"][value="quiz"]'
    ).check();

    cy.get(
      'input[type="checkbox"][name="settings.challengeSettings.enabledChallenges"][value="codeeval"]'
    ).check();

    cy.get(
      'input[type="checkbox"][name="settings.challengeSettings.enabledChallenges"][value="projecteval"]'
    ).check();

    cy.get(
      'div.relative.inline-block.w-10.mr-2.align-middle.select-none.transition.duration-200.ease-in input[type="checkbox"][id="settings.uiSettings.profileSocialLinks"]'
    ).check();
    cy.get(
      'div.relative.inline-block.w-10.mr-2.align-middle.select-none.transition.duration-200.ease-in input[type="checkbox"][id="settings.uiSettings.resumeAccessible"]'
    ).check();

    cy.contains("Submit").click();
    cy.contains(msgError.fieldRequired.msg).should("be.visible");
  });
});
