describe("organization", () => {
  let msgError;
  beforeEach(() => {
    cy.signInAsAdmin();

    cy.fixture("msg-error.json").then((data) => {
      msgError = data;
    });
  });

  it("should successfully create a organization delete quiz", () => {
    cy.get("#pencil").click({ force: true });
    cy.get("input#settings.challengeSettings.quizSuccessThreshold").clear();
    cy.contains("Submit").click();
    cy.contains(msgError.quizError.msg).should("be.visible");
  });
});
