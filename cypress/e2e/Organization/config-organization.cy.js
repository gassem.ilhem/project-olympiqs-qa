describe("organization", () => {
  let organization;

  beforeEach(() => {
    cy.signInAsManager();

    cy.fixture("organization.json").then((data) => {
      organization = data;
    });
  });

  it("should successfully configuration a organization", () => {
    cy.navigateToOrganizationPage();

    cy.get("input#displayName").type(
      organization.organizationConfig.displayName
    );

    cy.contains("Submit").click();
    cy.contains("Operation saved successfully").should("be.visible");
  });
});
