describe("organization", () => {
  let organization;

  beforeEach(() => {
    cy.signInAsAdmin();

    cy.fixture("organization.json").then((data) => {
      organization = data;
    });
  });

  it("should successfully update a organization", () => {
    cy.get("#pencil").last().click({ force: true });

    cy.get("input#displayName").type(
      organization.organizationUpdate.displayName
    );

    cy.contains("Submit").click();
    cy.url().should(
      "eq",
      "https://qa.olympiqs.com/administration/organizations"
    );
    cy.contains("Operation saved successfully").should("be.visible");
  });
});
