describe("organization", () => {
  beforeEach(() => {
    cy.signInAsAdmin();
  });

  it("should successfully see users of a organization", () => {
    cy.get(
      "#root > div.mx-auto.flex > div.w-full > div > section > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > a:nth-child(1) > svg"
    )
      .last()
      .click({ force: true });
    cy.url().should("include", "/users");
  });
});
