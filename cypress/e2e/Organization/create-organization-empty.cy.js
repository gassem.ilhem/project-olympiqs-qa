describe("organization", () => {
  let msgError;
  let organization;

  beforeEach(() => {
    cy.signInAsAdmin();
    cy.fixture("organization.json").then((data) => {
      organization = data;
    });
    cy.fixture("msg-error.json").then((data) => {
      msgError = data;
    });
  });

  it("should successfully create a organization empty", () => {
    cy.contains("Create Organization").click();

    cy.contains("Submit").click();
    cy.contains(msgError.fieldRequired.msg).should("be.visible");
    cy.contains(msgError.noChallenge.msg).should("be.visible");
    cy.url().should("include", "/create");
  });
});
