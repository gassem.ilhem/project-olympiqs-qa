describe("organization", () => {
  let msgError;
  beforeEach(() => {
    cy.signInAsManager();
    cy.fixture("msg-error.json").then((data) => {
      msgError = data;
    });
  });

  it("should configuration a organization delete displayname", () => {
    cy.navigateToOrganizationPage();

    cy.get("input#displayName").clear();

    cy.contains("Submit").click();
    cy.contains(msgError.fieldRequired.msg).should("be.visible");
  });
});
