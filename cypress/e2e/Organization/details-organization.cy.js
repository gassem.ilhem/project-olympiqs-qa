describe("organization", () => {
  beforeEach(() => {
    cy.signInAsAdmin();
  });

  it("should successfully see detail a organization", () => {
    cy.get(
      "#root > div.mx-auto.flex > div.w-full > div > section > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > div > svg"
    ).click({ force: true });
    cy.get(
      'div[aria-modal="true"][role="dialog"][aria-label="Details"]'
    ).should("be.visible");
  });
});
