describe("tags", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it(" create a tag without name", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.navigateToTagsPage();
    cy.contains("Create").click();
    cy.url().should("include", "/create");
    cy.contains("Submit").should("be.disabled");
  });
});
