import { generateRandomName } from "../../utils/helper";
describe("tags", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it("should successfully create a tag", () => {
    const { nameTag } = generateRandomName();
    cy.signInAsManager(manager.username, manager.password);
    cy.navigateToTagsPage();
    cy.contains("Create").click();
    cy.url().should("include", "/create");
    cy.get('[data-cy="name"]').type(nameTag);
    cy.contains("Submit").click();
    cy.location("pathname", { timeout: 60000 }).should(
      "eq",
      "/management/tags"
    );
    cy.searchAndSubmit(nameTag);
    cy.contains(nameTag).should("be.visible");
  });
});
