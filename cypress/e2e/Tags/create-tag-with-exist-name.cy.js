describe("tags", () => {
  let message;
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
        message = data.errorMessages;
      });
    });
  });
  it(" create a tag with exist name", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createTagApi().then(function (responseTag) {
      cy.navigateToTagsPage();
      cy.contains("Create").click();
      cy.url().should("include", "/create");
      cy.get('[data-cy="name"]').type(responseTag.body.name);
      cy.contains("Submit").click();
      cy.contains(message.tagExist).should("be.visible");
      cy.url().should("include", "/create");
    });
  });
});
