describe("tags", () => {
  let message;
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
        message = data.errorMessages;
      });
    });
  });
  it(" delete a tag with user", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createTagApi().then((responseTag) => {
      cy.createPlayerApiOutTag(responseTag.body.id).then((response) => {
        cy.navigateToTagsPage();
        cy.searchAndSubmit(responseTag.body.name);
        cy.get("#delete").click({ force: true });
        cy.get(".bg-primary").should("be.visible");
        cy.contains("Confirm").click();
        cy.contains(message.tagRequired).should("be.visible");
        cy.videSearch();
        cy.searchAndSubmit(responseTag.body.name);
        cy.contains(responseTag.body.name).should("be.visible");
      });
    });
  });
});
