describe("tags", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it(" delete a tag", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createTagApi().then(function (responseTag) {
      cy.navigateToTagsPage();

      cy.searchAndSubmit(responseTag.body.name);
      cy.get("#delete").click({ force: true });
      cy.get(".bg-primary").should("be.visible");
      cy.contains("Confirm").click();
      cy.videSearch();
      cy.searchAndSubmit(responseTag.body.name);
      cy.contains(responseTag.body.name).should("not.exist");
    });
  });
});
