describe("tags", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it("should successfully search a tag", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createTagApi().then(function (responseTag) {
      cy.navigateToTagsPage();
      cy.get('input[name="term"]').type(`${responseTag.body.name}{enter}`);
      cy.contains(responseTag.body.name).should("be.visible");
    });
  });
});
