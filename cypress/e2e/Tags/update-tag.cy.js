describe("tags", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it("should successfully update a tag", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createTagApi().then(function (responseTag) {
      cy.navigateToTagsPage();
      cy.searchAndSubmit(responseTag.body.name);
      cy.getByDataCy(responseTag.body.name + "-edit").click({ force: true });

      cy.get('[data-cy="name"]').should("have.value", responseTag.body.name);
      cy.get('[data-cy="name"]')
        .clear()
        .type("edited " + responseTag.body.name);
      cy.contains("Submit").click();
      cy.location("pathname", { timeout: 60000 }).should(
        "eq",
        "/management/tags"
      );
      cy.contains("edited " + responseTag.body.name).should("exist");
    });
  });
});
