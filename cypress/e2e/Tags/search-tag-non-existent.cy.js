import { generateRandomName } from "../../utils/helper";
describe("tags", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });

  it("should successfully search non exist tag", () => {
    const { nameTag } = generateRandomName();
    cy.signInAsManager(manager.username, manager.password);
    cy.navigateToTagsPage();

    cy.get('input[name="term"]').type(`${nameTag}{enter}`);
    cy.contains(nameTag).should("not.exist");
  });
});
