import { generateRandomName } from "../../utils/helper";
describe("sessions", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it("should creat a session type competition", () => {
    const { nameSession, description } = generateRandomName();
    cy.signInAsManager(manager.username, manager.password);
    cy.createCommunityApi().then(function (communityResponse) {
      cy.createQuizChallengeApiExemple().then(function (challengeResponse) {
        cy.contains("Create Session").click();
        cy.url().should("include", "/create");
        cy.contains("Next").click();
        cy.contains("Session details").parent().should("have.class", "active");
        cy.get('[data-cy="title"]').type(nameSession);
        cy.get('[data-cy="search-undefined"]').type(
          communityResponse.body.name
        );
        cy.contains(communityResponse.body.name).click();
        cy.get('[data-cy="description"]').type(description);
        cy.contains("Next").click();
        cy.contains("Manage challenges")
          .parent()
          .should("have.class", "active");
        cy.contains("Add Challenge").click();
        cy.get('[data-cy="search-undefined"]').type(
          challengeResponse.body.title,
          { force: true }
        );
        cy.contains(challengeResponse.body.title).click();
        cy.contains("Submit").click();
        cy.location("pathname").should("eq", "/management/competitions");
        cy.searchAndSubmit(nameSession);
        cy.contains(nameSession).should("be.visible");
      });
    });
  });
});
