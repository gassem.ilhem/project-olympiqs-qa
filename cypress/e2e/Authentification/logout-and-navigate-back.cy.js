describe("Authentification", () => {
  let userdata;
  let bouton;
  beforeEach(() => {
    cy.clearToken();
    cy.fixture("auth").then((data) => {
      userdata = data.manager;
    });
    cy.fixture("translate").then((data) => {
      bouton = data.buttons;
    });
  });
  it("should successfully logout and prevent reconnection with browser back button", () => {
    cy.visit("/");
    cy.location("pathname", { timeout: 60000 }).should("eq", "/login");
    cy.getByDataCy("username").type(userdata.username);
    cy.getByDataCy("password").type(userdata.password);
    cy.contains(bouton.SignIn).click();
    cy.location("pathname").should("eq", "/management/competitions");
    cy.getCookie("__access_token__").should("exist");
    cy.get(".css-15knso2.active").click();
    cy.wait(2000);
    cy.contains("Logout").click({ force: true });
    cy.wait(2000);
    cy.location("pathname").should("eq", "/login");
    cy.getCookie("__access_token__").should("not.exist");
    cy.go("back");
    cy.location("pathname").should("eq", "/login");
    cy.getCookie("__access_token__").should("not.exist");
  });
});
