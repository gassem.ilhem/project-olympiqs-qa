import { generatePassword, generateRandomName } from "../../utils/helper";
describe("Authentification", () => {
  let { firstName } = generateRandomName();
  let serverId = Cypress.env("MAILOSAUR_SERVER_ID");
  let testEmail = firstName + "@" + Cypress.env("MAILOSAUR_DOMAIN");
  let message;
  beforeEach(() => {
    cy.fixture("translate").then((data) => {
      message = data.errorMessages;
    });
    cy.InvitePlayer(testEmail).then(() => {
      cy.clearToken();
      cy.visit("/");
      cy.location("pathname", { timeout: 60000 }).should("eq", "/login");
      cy.contains("Forgot password ?").click();
      cy.location("pathname", { timeout: 60000 }).should(
        "eq",
        "/forgot-password"
      );
      cy.getByDataCy("email").type(testEmail);
      cy.contains("Submit").click();
    });
  });
  it("should reset password confirm password wrong", () => {
    const password = generatePassword();
    const password2 = generatePassword();
    const testStart = new Date();
    cy.mailosaurGetMessage(
      serverId,
      { sentTo: testEmail },
      {
        receivedAfter: testStart, // body
        apiKey: Cypress.env("MAILOSAUR_API_KEY"),
      }
    ).then((email) => {
      expect(email.subject).to.equal("Reset password");
      let resetLink = email.html.links[0].href;
      cy.visit(resetLink);
    });
    cy.location("pathname", { timeout: 60000 }).should(
      "include",
      "reset-password"
    );
    cy.getByDataCy("plainPassword").type(password2);
    cy.getByDataCy("confirmPlainPassword").type(password);
    cy.get(".text-red-500.text-xs.italic")
      .should("be.visible")
      .and("contain", message.PasswordDoNotMatch);
  });
});
