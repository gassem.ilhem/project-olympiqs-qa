describe("Authentification", () => {
  let userdata;
  let bouton;
  beforeEach(() => {
    cy.clearToken();
    cy.fixture("auth.json").then((data) => {
      userdata = data;
    });
    cy.fixture("translate").then((data) => {
      bouton = data.buttons;
    });
  });
  it("forget password", () => {
    cy.clearToken();
    cy.visit("/");
    cy.location("pathname", { timeout: 60000 }).should("eq", "/login");
    cy.contains(bouton.ForgotPassword).click();
    cy.location("pathname").should("eq", "/forgot-password");
    cy.getByDataCy("email").type(userdata.forgetPassword.username);
    cy.contains(bouton.SignIn).click();
    cy.location("pathname").should("eq", "/login");
  });
});
