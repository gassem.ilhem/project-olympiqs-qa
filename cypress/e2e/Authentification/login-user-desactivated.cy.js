import {
  generateRandomName,
  generatePassword,
  generateRandomEmail,
} from "../../utils/helper";
describe("Authentification", () => {
  let userdata;
  let bouton;
  let message;
  beforeEach(() => {
    cy.clearToken();
    cy.fixture("auth").then((data) => {
      userdata = data.manager;
    });
    cy.fixture("translate").then((data) => {
      bouton = data.buttons;
      message = data.errorMessages;
    });
  });
  it("should successfully login a desactivated user", () => {
    const password = generatePassword();
    const { firstName, lastName } = generateRandomName();
    const email = generateRandomEmail("player");
    cy.loginAPI(userdata.username, userdata.password);
    cy.createPlayerApi(firstName, lastName, email).then((player) => {
      cy.getUserApi(player.body.data[0].email).then((playerAct) => {
        cy.activatePlayerPendingPassword(
          playerAct.body.data[0].id,
          password,
          password
        ).then(() => {
          cy.desactivatePlayer(playerAct.body.data[0].id).then(() => {
            cy.clearToken();
            cy.visit("/");
            cy.location("pathname", { timeout: 60000 }).should("eq", "/login");
            cy.getByDataCy("username").type(player.body.data[0].email);
            cy.getByDataCy("password").type(password);
            cy.contains(bouton.SignIn).click();
            cy.get("p.text-center.text-danger").should(
              "contain",
              message.invalidOrLoginPass
            );
            cy.getCookie("__access_token__").should("not.exist");
          });
        });
      });
    });
  });
});
