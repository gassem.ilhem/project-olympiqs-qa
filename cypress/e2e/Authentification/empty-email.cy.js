describe("Authentification", () => {
  let userdata;
  let bouton;
  let message;
  beforeEach(() => {
    cy.clearToken();
    cy.fixture("auth.json").then((data) => {
      userdata = data;
    });
    cy.fixture("translate").then((data) => {
      bouton = data.buttons;
      message = data.errorMessages;
    });
  });
  it("should shows error messages for empty username ", () => {
    cy.visit("/");
    cy.location("pathname", { timeout: 60000 }).should("eq", "/login");
    cy.getByDataCy("username").clear();
    cy.getByDataCy("password").type(userdata.emptyEmail.password);
    cy.contains(bouton.SignIn).click();
    cy.getByDataCy("username")
      .parent()
      .next("small")
      .should("contain", message.fieldRequired);
    cy.getCookie("__access_token__").should("not.exist");
  });
});
