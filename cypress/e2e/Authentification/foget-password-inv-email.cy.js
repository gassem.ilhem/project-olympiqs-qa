describe("Authentification", () => {
  let userdata;
  let bouton;
  let message;
  beforeEach(() => {
    cy.clearToken();
    cy.fixture("auth.json").then((data) => {
      userdata = data.forgetPasswordInv;
    });
    cy.fixture("translate").then((data) => {
      bouton = data.buttons;
      message = data.errorMessages;
    });
  });
  it("should shows an error message to forget password invalid email", () => {
    cy.clearToken();
    cy.visit("/");
    cy.location("pathname", { timeout: 60000 }).should("eq", "/login");
    cy.contains("Forgot password ?").click();
    cy.location("pathname").should("eq", "/forgot-password");
    cy.getByDataCy("email").type(userdata.username);
    cy.contains(bouton.submit).click();
    cy.contains(message.invalidEmailForget).should("be.visible");
    cy.location("pathname").should("eq", "/forgot-password");
  });
});
