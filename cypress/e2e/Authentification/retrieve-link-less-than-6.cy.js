describe("Authentification", () => {
  let userdata;
  let msgError;
  beforeEach(() => {
    cy.clearToken();
    cy.fixture("auth.json").then((data) => {
      userdata = data;
    });
    cy.fixture("msg-error.json").then((data) => {
      msgError = data;
    });
  });

  //Rest password less than 6 characteres
  it("Rest password less than 6 characteres ", () => {
    cy.forgetPasswordLink();
    cy.location("pathname").should("include", "reset-password");
    cy.getByDataCy("plainPassword").type(
      userdata.ResetPasswordShort.plainPassword
    );
    cy.get("div.text-gray-500.font-medium.text-sm.ml-3.py-2.leading-none")
      .should("be.visible")
      .and("contain", msgError.TooWeak.msg);
    cy.get("small.text-red-500.text-xs.italic")
      .should("be.visible")
      .and("contain", msgError.sixCharacteres.msg);
  });
});
