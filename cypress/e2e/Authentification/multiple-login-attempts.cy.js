describe("Authentification", () => {
  let userdata;
  let bouton;
  let message;
  beforeEach(() => {
    cy.clearToken();
    cy.fixture("auth").then((data) => {
      userdata = data.invalidManager;
    });
    cy.fixture("translate").then((data) => {
      bouton = data.buttons;
      message = data.errorMessages;
    });
  });
  it("Should allow three login attempts before successful login", () => {
    cy.visit("/");
    cy.location("pathname", { timeout: 60000 }).should("eq", "/login");
    for (let i = 0; i < 3; i++) {
      cy.getByDataCy("username").type(userdata.username);
      cy.getByDataCy("password").type(userdata.password);
      cy.contains(bouton.SignIn).click();
      cy.get("p.text-center.text-danger", { timeout: 60000 }).should(
        "contain",
        message.invalidOrLoginPass
      );
      cy.getCookie("__access_token__").should("not.exist");
      cy.getByDataCy("username").clear();
      cy.getByDataCy("password").clear();
    }
  });
});
