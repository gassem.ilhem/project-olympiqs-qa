describe("Authentification", () => {
  let userdata;
  let bouton;
  let message;
  beforeEach(() => {
    cy.clearToken();
    cy.fixture("auth").then((data) => {
      userdata = data.invalidPassword;
    });
    cy.fixture("translate").then((data) => {
      bouton = data.buttons;
      message = data.errorMessages;
    });
  });
  it("should shows error message with invalid password", () => {
    cy.visit("/");
    cy.location("pathname", { timeout: 60000 }).should("eq", "/login");
    cy.getByDataCy("username").type(userdata.username);
    cy.getByDataCy("password").type(userdata.password);
    cy.contains(bouton.SignIn).click();
    cy.get("p.text-center.text-danger").should(
      "contain",
      message.invalidOrLoginPass
    );
    cy.getCookie("__access_token__").should("not.exist");
  });
});
