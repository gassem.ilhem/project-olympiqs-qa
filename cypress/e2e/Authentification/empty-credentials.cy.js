describe("Authentification", () => {
  let bouton;
  let message;
  beforeEach(() => {
    cy.clearToken();
    cy.fixture("translate").then((data) => {
      bouton = data.buttons;
      message = data.errorMessages;
    });
  });
  it("should shows error messages for empty username and password ", () => {
    cy.clearToken(); // Clear token initially
    cy.visit("/");
    cy.location("pathname", { timeout: 60000 }).should("eq", "/login");
    cy.location("pathname").should("eq", "/login");
    cy.getByDataCy("username").clear();
    cy.getByDataCy("password").clear();
    cy.contains(bouton.SignIn).click();
    cy.contains(message.fieldRequired).should("be.visible");
    cy.getCookie("__access_token__").should("not.exist");
  });
});
