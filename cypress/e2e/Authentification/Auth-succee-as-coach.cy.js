import { generatePassword } from "../../utils/helper";
describe("Authentification", () => {
  let userdata;
  let roleUser;
  let bouton;
  beforeEach(() => {
    cy.clearToken();
    cy.fixture("auth.json").then((data) => {
      userdata = data;
      roleUser = data.role;
    });
    cy.fixture("translate").then((data) => {
      bouton = data.buttons;
    });
  });
  it.only("sign in successfully as coach", () => {
    const password = generatePassword();
    console.log(password);
    cy.loginAPI("manager", "manager");
    cy.createCoachApi().then((coach) => {
      cy.getUserApi(coach.body.data[0].email).then((coachAct) => {
        cy.activatePlayerPendingPassword(
          coachAct.body.data[0].id,
          password,
          password
        ).then(() => {
          cy.clearToken();
          cy.visit("/");
          cy.location("pathname", { timeout: 60000 }).should("eq", "/login");
          cy.getByDataCy("username").type(coach.body.data[0].email);
          cy.getByDataCy("password").type(password);
          cy.contains(bouton.SignIn).click();
          cy.location("pathname").should("eq", "/management/competitions");
          cy.getCookie("__access_token__").should("exist");
          cy.get("span.css-3jv764").should("contain.text", roleUser.coach);
        });
      });
    });
  });
});
