describe("Authentification", () => {
  let userdata;
  let roleUser;
  let bouton;
  beforeEach(() => {
    cy.clearToken();
    cy.fixture("auth.json").then((data) => {
      userdata = data;
      roleUser = data.role;
    });
    cy.fixture("translate").then((data) => {
      bouton = data.buttons;
    });
  });
  //sign in as manager
  it("should sign in successfully as manager", () => {
    cy.visit("/");
    cy.location("pathname", { timeout: 60000 }).should("eq", "/login");
    cy.getByDataCy("username").type(userdata.manager.username);
    cy.getByDataCy("password").type(userdata.manager.password);
    cy.contains(bouton.SignIn).click();
    cy.location("pathname").should("eq", "/management/competitions");
    cy.getCookie("__access_token__").should("exist");
    cy.get("span.css-3jv764").should("contain.text", roleUser.manager);
  });
});
