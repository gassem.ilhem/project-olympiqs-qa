describe("Authentification", () => {
  let userdata;
  let roleUser;
  let bouton;
  beforeEach(() => {
    cy.clearToken();
    cy.fixture("auth.json").then((data) => {
      userdata = data;
      roleUser = data.role;
    });
    cy.fixture("translate").then((data) => {
      bouton = data.buttons;
    });
  });
  it("sigin in successfully as admin ", () => {
    cy.visit("https://qa.olympiqs.com/");
    cy.location("pathname", { timeout: 60000 }).should("eq", "/login");
    cy.getByDataCy("username", { timeout: 60000 }).type(
      userdata.admin.username
    );
    cy.getByDataCy("password", { timeout: 60000 }).type(
      userdata.admin.password
    );
    cy.contains(bouton.SignIn).click();
    cy.url().should(
      "eq",
      "https://qa.olympiqs.com/administration/organizations"
    );
    cy.getCookie("__access_token__").should("exist");
    cy.get("span.css-3jv764").should("contain.text", roleUser.admin);
  });
});
