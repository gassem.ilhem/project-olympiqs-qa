describe("Authentification", () => {
  let userdata;
  let bouton;
  let message;
  beforeEach(() => {
    cy.clearToken();
    cy.fixture("auth.json").then((data) => {
      userdata = data;
    });
    cy.fixture("translate").then((data) => {
      bouton = data.buttons;
      message = data.errorMessages;
    });
  });
  it("should shows error messages for empty password ", () => {
    cy.visit("/");
    cy.location("pathname", { timeout: 60000 }).should("eq", "/login");
    cy.getByDataCy("username").type(userdata.emptyPassword.username);
    cy.getByDataCy("password").clear();
    cy.contains(bouton.SignIn).click();
    cy.get("small.text-red-500").should("contain", message.fieldRequired);
    cy.getCookie("__access_token__").should("not.exist");
  });
});
