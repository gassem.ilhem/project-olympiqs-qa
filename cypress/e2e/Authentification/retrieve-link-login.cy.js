describe("Authentification", () => {
  let userdata;
  let msgError;
  beforeEach(() => {
    cy.clearToken();
    cy.fixture("auth.json").then((data) => {
      userdata = data;
    });
    cy.fixture("msg-error.json").then((data) => {
      msgError = data;
    });
  });

  //Rest password login
  it("Rest password login", () => {
    cy.forgetPasswordLink();
    cy.location("pathname").should("include", "reset-password");
    cy.clearToken();
    cy.getByDataCy("plainPassword").type(
      userdata.ResetPasswordStrong.plainPassword
    );
    cy.getByDataCy("confirmPlainPassword").type(
      userdata.confirmPlainPassword.plainPassword
    );
    cy.get("button.css-1lzm341").contains("Submit").click();
    cy.location("pathname").should("eq", "/login");
    cy.getByDataCy("username").type(userdata.forgetPasswordLogin.username);
    cy.getByDataCy("password").type(userdata.forgetPasswordLogin.password);
    cy.contains(bouton.SignIn).click();
    cy.location("pathname").should("eq", "/competitions");
    cy.getCookie("__access_token__").should("exist");
  });
});
