describe("Authentification", () => {
  let userdata;
  let msgError;
  beforeEach(() => {
    cy.clearToken();
    cy.fixture("auth.json").then((data) => {
      userdata = data;
    });
    cy.fixture("msg-error.json").then((data) => {
      msgError = data;
    });
  });

  //Rest password more than 6 characteres with special charac
  it("Rest password more than 6 characteres with special charac", () => {
    cy.forgetPasswordLink();
    cy.location("pathname").should("include", "reset-password");
    cy.clearToken();
    cy.getByDataCy("plainPassword").type(
      userdata.ResetPasswordCharSep.plainPassword
    );
    cy.get("div.text-gray-500.font-medium.text-sm.ml-3.py-2.leading-none")
      .should("be.visible")
      .and("contain", msgError.CouldBeStronger.msg);
  });
});
