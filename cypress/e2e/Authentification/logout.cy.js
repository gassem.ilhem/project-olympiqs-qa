describe("Authentification", () => {
  let userdata;
  let bouton;
  beforeEach(() => {
    cy.clearToken();
    cy.fixture("auth").then((data) => {
      userdata = data.manager;
    });
    cy.fixture("translate").then((data) => {
      bouton = data.buttons;
    });
  });
  it("Should successfully log out after logging in", () => {
    cy.clearToken();
    cy.visit("/");
    cy.location("pathname", { timeout: 60000 }).should("eq", "/login");
    cy.getByDataCy("username").type(userdata.username);
    cy.getByDataCy("password").type(userdata.password);
    cy.contains(bouton.SignIn).click();
    cy.location("pathname").should("eq", "/management/competitions");
    cy.getCookie("__access_token__").should("exist");
    cy.get(".css-15knso2.active").click();
    cy.contains("Logout", { timeout: 60000 }).click({ force: true });
    cy.location("pathname", { timeout: 60000 }).should("eq", "/login");
    cy.getCookie("__access_token__").should("not.exist");
  });
});
