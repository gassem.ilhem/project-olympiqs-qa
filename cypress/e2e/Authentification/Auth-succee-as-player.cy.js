import {
  generateRandomName,
  generatePassword,
  generateRandomEmail,
} from "../../utils/helper";
describe("Authentification", () => {
  let userdata;
  let roleUser;
  let bouton;
  beforeEach(() => {
    cy.clearToken();
    cy.fixture("auth.json").then((data) => {
      userdata = data;
      roleUser = data.role;
    });
    cy.fixture("translate").then((data) => {
      bouton = data.buttons;
    });
  });
  it("should sign in successfully as player", () => {
    const password = generatePassword();
    const { firstName, lastName } = generateRandomName();
    const email = generateRandomEmail("player");
    cy.loginAPI(userdata.manager.username, userdata.manager.password);
    cy.createPlayerApi(firstName, lastName, email).then((player) => {
      cy.getUserApi(player.body.data[0].email).then((playerAct) => {
        cy.activatePlayerPendingPassword(
          playerAct.body.data[0].id,
          password,
          password
        ).then(() => {
          cy.clearToken();
          cy.visit("/");
          cy.location("pathname", { timeout: 60000 }).should("eq", "/login");
          cy.getByDataCy("username").type(player.body.data[0].email);
          cy.getByDataCy("password").type(password);
          cy.contains(bouton.SignIn).click();
          cy.location("pathname").should("eq", "/competitions");
          cy.getCookie("__access_token__").should("exist");
          cy.get("span.css-3jv764").should("contain.text", roleUser.player);
        });
      });
    });
  });
});
