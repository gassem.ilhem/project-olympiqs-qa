describe("community", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it("should successfully delete a community", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createCommunityApi().then(function (firstResponse) {
      cy.navigateToCommunitiesPage();

      cy.searchAndSubmit(firstResponse.body.name);
      cy.get("#delete").click({ force: true });
      cy.get(".bg-primary").should("be.visible");
      cy.contains("Submit").click();
      cy.videSearch();
      cy.searchAndSubmit(firstResponse.body.name);
      cy.contains(firstResponse.body.name).should("not.exist");
    });
  });
});
