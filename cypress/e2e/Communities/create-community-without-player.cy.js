import { generateRandomName } from "../../utils/helper";
describe("community", () => {
  let message;
  let manager;
  let bouton;
  let community;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
        message = data.errorMessages;
      });
      cy.fixture("community").then((data) => {
        community = data;
      });
    });
  });
  it("should create a community without player", () => {
    const { nameCommunity, description } = generateRandomName();
    cy.signInAsManager(manager.username, manager.password);
    cy.navigateToCommunitiesPage();
    cy.contains("Create community").click();
    cy.url().should("include", "/create");
    cy.contains("Basic informations").parent().should("have.class", "active");
    cy.getByDataCy("name").type(nameCommunity);
    cy.getByDataCy("description").type(description);
    cy.contains("Next").click();
    cy.contains("Manage members").parent().should("have.class", "active");
    cy.get("input#search").first().type(community.communityCreate.Managers);
    cy.contains(community.communityCreate.Managers).click();
    cy.contains("Submit").click();
    cy.location("pathname", { timeout: 60000 }).should(
      "eq",
      "/management/communities"
    );
    cy.searchAndSubmit(nameCommunity);
    cy.contains(nameCommunity).should("be.visible");
  });
});
