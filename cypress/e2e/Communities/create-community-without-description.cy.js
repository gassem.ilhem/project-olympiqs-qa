import { generateRandomName } from "../../utils/helper";
describe("community", () => {
  let message;
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
        message = data.errorMessages;
      });
    });
  });
  it("should create a community without description", () => {
    const { nameCommunity } = generateRandomName();
    cy.signInAsManager(manager.username, manager.password);
    cy.navigateToCommunitiesPage();
    cy.contains("Create community").click();
    cy.url().should("include", "/create");
    cy.contains("Basic informations").parent().should("have.class", "active");
    cy.getByDataCy("name").type(nameCommunity);
    cy.contains("Next").click();
    cy.contains(message.fieldRequired).should("be.visible");
    cy.url().should("include", "/create");
  });
});
