describe("community", () => {
  let manager;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
    });
  });
  it("should successfully analytics a community", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.navigateToCommunitiesPage();
    cy.get(
      "#root > div.mx-auto.flex > div.w-full > div > section > table > tbody > tr > td:nth-child(8) > div > a:nth-child(4) > div > svg",
      { timeout: 60000 }
    )
      .first()
      .click({ force: true });
    cy.url().should("include", "/analytics");
  });
});
