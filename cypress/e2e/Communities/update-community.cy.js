describe("community", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it("should successfully update a community", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createCommunityApi().then(function (firstResponse) {
      cy.navigateToCommunitiesPage();

      cy.searchAndSubmit(firstResponse.body.name);
      cy.get("#pencil").click({ force: true });
      cy.url().should("include", "/update");
      cy.contains("Basic informations").parent().should("have.class", "active");
      cy.get('[data-cy="name"]').should("have.value", firstResponse.body.name);
      cy.get('[data-cy="name"]')
        .clear()
        .type("edited " + firstResponse.body.name);
      cy.contains("Next").click();
      cy.contains("Submit").click();
      cy.contains("edited " + firstResponse.body.name).should("exist");
    });
  });
});
