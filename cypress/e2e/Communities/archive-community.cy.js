describe("community", () => {
  let manager;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
    });
  });
  it("should successfully archive a community", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createCommunityApi().then(function (firstResponse) {
      cy.navigateToCommunitiesPage();
      cy.searchAndSubmit(firstResponse.body.name);
      cy.getByDataCy(firstResponse.body.name + "-archive")
        .first()
        .click({ force: true });
      cy.get(".shadow-md").should("be.visible");
      cy.contains("Submit").click();
      cy.videSearch();
      cy.searchAndSubmit(firstResponse.body.name);
      cy.contains("archived").should("be.visible");
    });
  });
});
