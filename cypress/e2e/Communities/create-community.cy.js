import { generateRandomName, generateRandomEmail } from "../../utils/helper";
describe("community", () => {
  let message;
  let manager;
  let bouton;
  let community;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
        message = data.errorMessages;
      });
      cy.fixture("community").then((data) => {
        community = data;
      });
    });
  });
  it("should successfully create a community", () => {
    const { firstName, lastName, nameCommunity, description } =
      generateRandomName();
    const email = generateRandomEmail("player");
    cy.signInAsManager(manager.username, manager.password);
    cy.createPlayerApi(firstName, lastName, email).then(function (
      firstResponse
    ) {
      cy.navigateToCommunitiesPage();

      cy.contains("Create community").click();

      cy.url().should("include", "/create");
      cy.contains("Basic informations").parent().should("have.class", "active");
      cy.get('[data-cy="name"]').type(nameCommunity);
      cy.get('[data-cy="description"]').type(description);
      cy.contains("Next").click();
      cy.contains("Manage members").parent().should("have.class", "active");
      cy.get("input#search").first().type(community.communityCreate.Managers);
      cy.contains(community.communityCreate.Managers).click();

      cy.xpath(
        "/html/body/div[1]/div[1]/div[4]/div/div/div[2]/div/div/form/div[2]/div/div/div/div/div/div/div/div/div/input"
      ).type(firstResponse.body.data[0].email);
      cy.contains(firstResponse.body.data[0].email).click();
      cy.contains("Submit").click();
      cy.location("pathname", { timeout: 60000 }).should(
        "eq",
        "/management/communities"
      );
      cy.searchAndSubmit(nameCommunity);
      cy.contains(nameCommunity).should("be.visible");
    });
  });
});
