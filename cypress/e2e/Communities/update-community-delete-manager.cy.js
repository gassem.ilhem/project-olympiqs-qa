describe("community", () => {
  let message;
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
        message = data.errorMessages;
      });
    });
  });
  it("should update a community with deleting name", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createCommunityApi().then(function (firstResponse) {
      cy.navigateToCommunitiesPage();

      cy.searchAndSubmit(firstResponse.body.name);
      cy.get("#pencil").click({ force: true });
      cy.url().should("include", "/update");
      cy.contains("Basic informations").parent().should("have.class", "active");

      cy.contains("Next").click();
      cy.get(".mr-2 > .flex > .cursor-pointer").click();
      cy.contains("Submit").click();
      cy.contains(message.managerRequired).should("be.visible");
      cy.url().should("include", "/update");
    });
  });
});
