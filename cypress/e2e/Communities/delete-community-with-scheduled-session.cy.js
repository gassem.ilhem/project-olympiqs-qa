describe("community", () => {
  let message;
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
        message = data.errorMessages;
      });
    });
  });
  it("should delete a community with scheduled session", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createSessionCompetitionApi().then(function (firstResponse) {
      cy.navigateToCommunitiesPage();
      cy.searchAndSubmit(firstResponse.body.community.name);
      cy.getByDataCy(firstResponse.body.community.name + "-delete").click({
        force: true,
      });
      cy.contains("Submit").click();
      cy.contains(message.communityScheduled).should("be.visible");
      cy.videSearch();
      cy.searchAndSubmit(firstResponse.body.community.name);
      cy.contains(firstResponse.body.community.name).should("be.visible");
    });
  });
});
