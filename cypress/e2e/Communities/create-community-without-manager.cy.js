import { generateRandomName, generateRandomEmail } from "../../utils/helper";
describe("community", () => {
  let message;
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
        message = data.errorMessages;
      });
    });
  });
  it("should create a community without manager", () => {
    const { firstName, lastName, nameCommunity, description } =
      generateRandomName();
    const email = generateRandomEmail("player");
    cy.signInAsManager(manager.username, manager.password);
    cy.createPlayerApi(firstName, lastName, email).then(function (
      firstResponse
    ) {
      cy.navigateToCommunitiesPage();
      cy.contains("Create community").click();
      cy.url().should("include", "/create");
      cy.contains("Basic informations").parent().should("have.class", "active");
      cy.getByDataCy("name").type(nameCommunity);
      cy.getByDataCy("description").type(description);
      cy.contains("Next").click();
      cy.contains("Manage members").parent().should("have.class", "active");
      cy.xpath(
        "/html/body/div[1]/div[1]/div[4]/div/div/div[2]/div/div/form/div[2]/div/div/div/div/div/div/div/div/div/input"
      ).type(firstResponse.body.data[0].email);
      cy.contains(firstResponse.body.data[0].email).click();
      cy.contains("Submit").click();
      cy.contains(message.managerRequired).should("be.visible");
      cy.url().should("include", "/create");
    });
  });
});
