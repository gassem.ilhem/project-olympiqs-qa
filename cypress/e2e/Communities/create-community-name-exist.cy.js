describe("community", () => {
  let community;
  let message;
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("community.json").then((data) => {
        community = data;
      });
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
        message = data.errorMessages;
      });
    });
  });
  it("should create a community with name already exist", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.createCommunityApi().then(function (firstResponse) {
      cy.navigateToCommunitiesPage();
      cy.contains("Create community").click();
      cy.url().should("include", "/create");
      cy.contains("Basic informations").parent().should("have.class", "active");
      cy.getByDataCy("name").type(firstResponse.body.name);
      cy.getByDataCy("description").type(community.communityCreate.Description);
      cy.contains("Next").click();
      cy.contains("Manage members").parent().should("have.class", "active");
      cy.get("input#search").first().type(community.communityCreate.Managers);
      cy.contains(community.communityCreate.Managers).click();
      cy.contains("Submit").click();
      cy.contains(message.somethingWentWrong).should("be.visible");
      cy.url().should("include", "/create");
    });
  });
});
