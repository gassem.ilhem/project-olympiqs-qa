import { generateRandomName, generateRandomEmail } from "../../utils/helper";
describe("users", () => {
  let users;
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
      cy.fixture("users.json").then((data) => {
        users = data;
      });
    });
  });
  it("should add multiple player successfully", () => {
    const { firstName, lastName } = generateRandomName();
    const email = generateRandomEmail("player");
    const email2 = generateRandomEmail("player");
    cy.signInAsManager(manager.username, manager.password);
    cy.createCommunityApi().then(function (firstResponse) {
      cy.createTagApi().then(function (responseTag) {
        cy.navigateToUsersPage();

        cy.contains("Invite users").click();

        cy.url().should("include", "/create");
        cy.get('[data-cy="users[0].firstName"]').eq(0).type(firstName);
        cy.get('[data-cy="users[0].lastName"]').eq(0).type(lastName);
        cy.get('[data-cy="users[0].email"]').eq(0).clear();
        cy.get('[data-cy="users[0].email"]').eq(0).type(email);
        cy.xpath(
          "/html/body/div[1]/div[1]/div[4]/div/div/div/div/div[2]/div[1]/div/form/div[2]/div[2]/div/div[4]/div/div/div/div/div/div/div/input"
        ).type(firstResponse.body.name);
        cy.contains(firstResponse.body.name).click();
        cy.xpath(
          "/html/body/div[1]/div[1]/div[4]/div/div/div/div/div[2]/div[1]/div/form/div[2]/div[2]/div/div[5]/div/div/div/div/div/div/div/input"
        ).type(responseTag.body.name);
        cy.contains(responseTag.body.name).click();

        cy.xpath(
          "/html/body/div[1]/div[1]/div[4]/div/div/div/div/div[2]/div[1]/div/form/button",
          { timeout: 600000 }
        ).click();
        cy.get('[data-cy="users[1].firstName"]').eq(0).type(firstName);
        cy.get('[data-cy="users[1].lastName"]').eq(0).type(lastName);
        cy.get('[data-cy="users[1].email"]').eq(0).clear();
        cy.get('[data-cy="users[1].email"]').eq(0).type(email2);
        cy.xpath(
          "/html/body/div[1]/div[1]/div[4]/div/div/div/div/div[2]/div[1]/div/form/div[3]/div[2]/div/div[4]/div/div/div/div/div/div/div/input"
        ).type(firstResponse.body.name);
        cy.contains(firstResponse.body.name).click();

        cy.xpath(
          "/html/body/div[1]/div[1]/div[4]/div/div/div/div/div[2]/div[1]/div/form/div[3]/div[2]/div/div[5]/div/div/div/div/div/div/div/input"
        ).type(responseTag.body.name);
        cy.contains(responseTag.body.name).click();

        cy.contains("Submit").click();

        cy.location("pathname", { timeout: 60000 }).should(
          "eq",
          "/management/users"
        );
        cy.searchAndSubmit(users.player2.email);
        cy.contains(users.player2.email).should("be.visible");
        cy.videSearch();
        cy.searchAndSubmit(users.player3.email);
        cy.contains(users.player3.email).should("be.visible");
      });
    });
  });
});
