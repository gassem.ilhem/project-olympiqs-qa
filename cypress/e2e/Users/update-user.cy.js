import { generateRandomName, generateRandomEmail } from "../../utils/helper";
describe("users", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it("should update user", () => {
    const { firstName, lastName } = generateRandomName();
    const email = generateRandomEmail("player");
    cy.signInAsManager(manager.username, manager.password);
    cy.createPlayerApi(firstName, lastName, email).then(function (
      firstResponse
    ) {
      cy.createTagApi().then(function (responseTag) {
        cy.navigateToUsersPage();

        cy.searchAndSubmit(firstResponse.body.data[0].email);
        cy.contains(firstResponse.body.data[0].email).should("be.visible", {
          timeout: 60000,
        });

        cy.get("#pencil").click({ force: true });
        cy.location("pathname", { timeout: 60000 }).should(
          "include",
          "/update"
        );
        cy.xpath(
          "/html/body/div[1]/div[1]/div[4]/div/section/div/div/div/form/div[4]/div/div/div/div/div/div/div/input"
        ).type(responseTag.body.name);
        cy.contains(responseTag.body.name).click();
        cy.contains("Submit").click();
        cy.location("pathname", { timeout: 60000 }).should(
          "eq",
          "/management/users"
        );
        cy.searchAndSubmit(firstResponse.body.data[0].email);
        cy.contains(firstResponse.body.data[0].email).should("be.visible");
      });
    });
  });
});
