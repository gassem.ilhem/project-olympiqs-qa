import { generateRandomName, generateRandomEmail } from "../../utils/helper";
describe("users", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it("should see details of a user", () => {
    const { firstName, lastName } = generateRandomName();
    const email = generateRandomEmail("player");
    cy.signInAsManager(manager.username, manager.password);
    cy.createPlayerApi(firstName, lastName, email).then(function (
      firstResponse
    ) {
      cy.navigateToUsersPage();
      cy.navigateToUsersPage();

      cy.searchAndSubmit(firstResponse.body.data[0].email);

      cy.get(
        "#root > div.mx-auto.flex > div.w-full > div > section > table > tbody > tr:nth-child(1) > td:nth-child(7) > div > div:nth-child(2)",
        { timeout: 60000 }
      ).click({ force: true });

      cy.location("pathname", { timeout: 60000 }).should("include", "/detail");
    });
  });
});
