import { generateRandomName, generateRandomEmail } from "../../utils/helper";
describe("users", () => {
  let firstResponse;
  before(() => {
    const { firstName, lastName } = generateRandomName();
    const email = generateRandomEmail("player");
    cy.loginAPI("manager", "manager");
    cy.createPlayerApi(firstName, lastName, email).then(function (response) {
      firstResponse = response;
    });
  });

  it("failed add player and manager with the same email", () => {
    cy.signInAsAdmin();
    cy.xpath(
      "/html/body/div[1]/div[1]/div[4]/div/section/table/tbody/tr[1]/td[3]/div/a[2]"
    ).click();
    cy.get('[data-cy="users[0].firstName"]')
      .eq(0)
      .type(firstResponse.body.data[0].firstName);
    cy.get('[data-cy="users[0].lastName"]')
      .eq(0)
      .type(firstResponse.body.data[0].lastName);
    cy.get('[data-cy="users[0].email"]').eq(0).clear();
    cy.get('[data-cy="users[0].email"]')
      .eq(0)
      .type(firstResponse.body.data[0].email);

    cy.xpath(
      "/html/body/div[1]/div[1]/div[4]/div/div/div/div/div/form/div[3]/button"
    ).click();
    cy.xpath("/html/body/reach-portal/div[2]/div/div/div/div").should(
      "be.visible"
    );
  });
});
