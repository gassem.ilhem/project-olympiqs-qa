import { generateRandomName, generateRandomEmail } from "../../utils/helper";
describe("users", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it("should see player anaytics", () => {
    const { firstName, lastName } = generateRandomName();
    const email = generateRandomEmail("player");
    cy.signInAsManager(manager.username, manager.password);
    cy.createPlayerApi(firstName, lastName, email).then(function (
      firstResponse
    ) {
      cy.navigateToUsersPage();

      cy.get(
        "#root > div.mx-auto.flex > div.w-full > div > section > header > a.css-n6y9a7 > svg"
      )
        .first()
        .click({ force: true });
      cy.location("pathname", { timeout: 60000 }).should(
        "include",
        "/analytics/players"
      );
    });
  });
});
