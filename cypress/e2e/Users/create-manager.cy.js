import { generateRandomName, generateRandomEmail } from "../../utils/helper";
describe("users", () => {
  let bouton;
  before(() => {
    cy.fixture("translate").then((data) => {
      bouton = data.buttons;
    });
  });
  it("should successfully create a manager", () => {
    const { firstName, lastName } = generateRandomName();
    const email = generateRandomEmail("coach");
    cy.signInAsAdmin();
    cy.xpath(
      "/html/body/div[1]/div[1]/div[4]/div/section/table/tbody/tr[1]/td[3]/div/a[2]"
    ).click();
    cy.get('[data-cy="users[0].firstName"]').eq(0).type(firstName);
    cy.get('[data-cy="users[0].lastName"]').eq(0).type(lastName);
    cy.get('[data-cy="users[0].email"]').eq(0).clear();
    cy.get('[data-cy="users[0].email"]').eq(0).type(email);
    cy.xpath(
      "/html/body/div[1]/div[1]/div[4]/div/div/div/div/div/form/div[3]/button"
    ).click();
    cy.url().should(
      "eq",
      "https://qa.olympiqs.com/administration/organizations"
    );
  });
});
