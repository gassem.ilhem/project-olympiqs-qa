import { generateRandomName, generateRandomEmail } from "../../utils/helper";
describe("users", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it('should activate a user with "disabled" status from the user list', () => {
    const { firstName, lastName } = generateRandomName();
    const email = generateRandomEmail("player");
    cy.signInAsManager(manager.username, manager.password);
    cy.createPlayerApi(firstName, lastName, email).then((player) => {
      cy.getUserApi(player.body.data[0].email).then((session) => {
        cy.activatePlayerPending(session.body.data[0].id).then(
          (sessionDate) => {
            cy.desactivatePlayer(session.body.data[0].id).then(
              (desactivate) => {
                cy.navigateToUsersPage();

                cy.searchAndSubmit(player.body.data[0].email);
                cy.contains(player.body.data[0].email).should("be.visible");

                cy.get(
                  "#root > div.mx-auto.flex > div.w-full > div > section > table > tbody > tr > td:nth-child(7) > div > span > div",
                  { timeout: 60000 }
                )
                  .first()
                  .click({ force: true });

                cy.contains("Submit").click();
                cy.videSearch();
                cy.searchAndSubmit(player.body.data[0].email);
                cy.contains("active").should("be.visible");
              }
            );
          }
        );
      });
    });
  });
});
