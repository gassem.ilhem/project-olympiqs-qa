import { generateRandomName, generateRandomEmail } from "../../utils/helper";
describe("users", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });

  it("should delete user", () => {
    const { firstName, lastName } = generateRandomName();
    const email = generateRandomEmail("player");
    cy.signInAsManager(manager.username, manager.password);
    cy.createPlayerApi(firstName, lastName, email).then(function (
      firstResponse
    ) {
      cy.navigateToUsersPage();
      cy.searchAndSubmit(firstResponse.body.data[0].email);
      cy.contains(firstResponse.body.data[0].email).should("be.visible", {
        timeout: 60000,
      });
      cy.get("#delete").click({ force: true });
      cy.get("button.css-10ruw0").click();
      cy.searchAndSubmit(firstResponse.body.data[0].email);
      cy.contains(firstResponse.body.data[0].email).should("not.exist", {
        timeout: 60000,
      });
    });
  });
});
