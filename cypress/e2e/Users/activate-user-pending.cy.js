import { generateRandomName, generateRandomEmail } from "../../utils/helper";
describe("users", () => {
  let users;
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
      cy.fixture("users.json").then((data) => {
        users = data;
      });
    });
  });
  it('should activate a user with "disabled" status from the user list', () => {
    const { firstName, lastName } = generateRandomName();
    const email = generateRandomEmail("player");
    cy.signInAsManager(manager.username, manager.password);
    cy.createPlayerApi(firstName, lastName, email).then(function (
      firstResponse
    ) {
      cy.navigateToUsersPage();

      cy.searchAndSubmit(firstResponse.body.data[0].email);
      cy.contains(firstResponse.body.data[0].email).should("be.visible");

      cy.get(
        "#root > div.mx-auto.flex > div.w-full > div > section > table > tbody > tr:nth-child(1) > td:nth-child(7) > div > div:nth-child(5)",
        { timeout: 60000 }
      ).click({ force: true });

      cy.get("body > reach-portal > div:nth-child(2) > div > div > div").should(
        "be.visible"
      );
      cy.get('[data-cy="plainPassword"]').type(users.userPending.mdp);
      cy.get('[data-cy="confirmPlainPassword"]').type(
        users.userPending.mdpConfirm
      );
      cy.contains("Submit").click();
      cy.videSearch();
      cy.searchAndSubmit(firstResponse.body.data[0].email);
      cy.contains("active").should("be.visible");
    });
  });
});
