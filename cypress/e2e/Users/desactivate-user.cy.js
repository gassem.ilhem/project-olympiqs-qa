import { generateRandomName, generateRandomEmail } from "../../utils/helper";
describe("users", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it("should desactivate user", () => {
    const { firstName, lastName } = generateRandomName();
    const email = generateRandomEmail("player");
    cy.signInAsManager(manager.username, manager.password);
    cy.createPlayerApi(firstName, lastName, email).then((player) => {
      cy.getUserApi(player.body.data[0].email).then((session) => {
        cy.activatePlayerPending(session.body.data[0].id).then(
          (sessionDate) => {
            cy.navigateToUsersPage();

            cy.searchAndSubmit(player.body.data[0].email);

            cy.xpath(
              "/html/body/div[1]/div[1]/div[4]/div/section/table/tbody/tr/td[7]/div/span/div",
              { timeout: 600000 }
            )
              .first()
              .click({ force: true });

            cy.contains("Submit").click();
            cy.videSearch();
            cy.searchAndSubmit(player.body.data[0].email);
            cy.contains("disabled").should("be.visible");
          }
        );
      });
    });
  });
});
