import { generateRandomName, generateRandomEmail } from "../../utils/helper";
describe("Users", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it("should contains all users", () => {
    const { firstName, lastName } = generateRandomName();
    const email = generateRandomEmail("player");
    cy.signInAsManager(manager.username, manager.password);
    cy.createCoachApi().then((coach) => {
      cy.createPlayerApi(firstName, lastName, email).then((player) => {
        cy.navigateToUsersPage();
        cy.searchAndSubmit(coach.body.data[0].email);
        cy.contains(coach.body.data[0].email).should("be.visible");
        cy.videSearch();
        cy.searchAndSubmit(player.body.data[0].email);
        cy.contains(player.body.data[0].email).should("be.visible");
      });
    });
  });
});
