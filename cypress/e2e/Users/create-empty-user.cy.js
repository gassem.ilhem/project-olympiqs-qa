describe("users", () => {
  let manager;
  let bouton;
  let message;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
        message = data.errorMessages;
      });
    });
  });
  it("should create an empty user", () => {
    cy.signInAsManager(manager.username, manager.password);
    cy.navigateToUsersPage();
    cy.contains("Invite users").click();
    cy.url().should("include", "/create");
    cy.get('[data-cy="users[0].email"]').eq(0).clear();
    cy.contains("Submit").click();
    cy.contains(message.invalidEmail).should("be.visible");
  });
});
