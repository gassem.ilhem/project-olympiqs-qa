import { generateRandomName, generateRandomEmail } from "../../utils/helper";
describe("users", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it("should successfully create a coach", () => {
    const { firstName, lastName } = generateRandomName();
    const email = generateRandomEmail("coach");
    cy.signInAsManager(manager.username, manager.password);
    cy.createCommunityApi().then(function (firstResponse) {
      cy.navigateToUsersPage();

      cy.contains("Invite users").click();

      cy.url().should("include", "/create");

      cy.contains("Invite Coaches").click();
      cy.xpath(
        "/html/body/div[1]/div[1]/div[4]/div/div/div/div/div[2]/div[2]/div/form/div[2]/div[2]/div/div[1]/label/div/input"
      )
        .first()
        .type(firstName, { force: true });
      cy.xpath(
        "/html/body/div[1]/div[1]/div[4]/div/div/div/div/div[2]/div[2]/div/form/div[2]/div[2]/div/div[2]/label/div/input"
      )
        .first()
        .type(lastName, { force: true });
      cy.xpath(
        "/html/body/div[1]/div[1]/div[4]/div/div/div/div/div[2]/div[2]/div/form/div[2]/div[2]/div/div[3]/label/div/input"
      )
        .first()
        .clear({ force: true });
      cy.xpath(
        "/html/body/div[1]/div[1]/div[4]/div/div/div/div/div[2]/div[2]/div/form/div[2]/div[2]/div/div[3]/label/div/input"
      )
        .first()
        .type(email, { force: true });
      cy.xpath(
        "/html/body/div[1]/div[1]/div[4]/div/div/div/div/div[2]/div[2]/div/form/div[2]/div[2]/div/div[4]/div/div/div/div/div/div/div/input"
      ).type(firstResponse.body.name);
      cy.contains(firstResponse.body.name).click();

      cy.xpath(
        "/html/body/div[1]/div[1]/div[4]/div/div/div/div/div[2]/div[2]/div/form/div[3]/button"
      ).click();

      cy.location("pathname", { timeout: 60000 }).should(
        "eq",
        "/management/users"
      );
      cy.searchAndSubmit(email);
      cy.contains(email).should("be.visible");
    });
  });
});
