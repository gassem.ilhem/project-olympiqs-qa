import { generateRandomEmail } from "../../utils/helper";
describe("users", () => {
  let manager;
  let bouton;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
      });
    });
  });
  it("should successfully create a player with only email", () => {
    const email = generateRandomEmail("player");
    cy.signInAsManager(manager.username, manager.password);
    cy.navigateToUsersPage();
    cy.contains("Invite users").click();
    cy.url().should("include", "/create");
    cy.get('[data-cy="users[0].email"]').eq(0).clear();
    cy.get('[data-cy="users[0].email"]').eq(0).type(email);
    cy.contains("Submit").click();
    cy.location("pathname", { timeout: 60000 }).should(
      "eq",
      "/management/users"
    );
    cy.searchAndSubmit(email);
    cy.contains(email).should("be.visible");
  });
});
