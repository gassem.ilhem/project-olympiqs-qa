import { generateRandomName, generateRandomEmail } from "../../utils/helper";
describe("users", () => {
  let manager;
  let bouton;
  let message;
  before(() => {
    cy.fixture("auth").then((auth) => {
      manager = auth.manager;
      cy.fixture("translate").then((data) => {
        bouton = data.buttons;
        message = data.errorMessages;
      });
    });
  });
  it("should resend invite user", () => {
    const { firstName, lastName } = generateRandomName();
    const email = generateRandomEmail("player");
    cy.signInAsManager(manager.username, manager.password);
    cy.createPlayerApi(firstName, lastName, email).then(function (
      firstResponse
    ) {
      cy.navigateToUsersPage();

      cy.searchAndSubmit(firstResponse.body.data[0].email);
      cy.get(
        "#root > div.mx-auto.flex > div.w-full > div > section > table > tbody > tr:nth-child(1) > td:nth-child(7) > div > div:nth-child(3) > svg",
        { timeout: 100000 }
      ).click({ force: true });
      cy.contains(message.msgSucc).should("be.visible");
    });
  });
});
