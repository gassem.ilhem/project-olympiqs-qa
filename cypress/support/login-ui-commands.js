let userdata;
let variable;
let bouton;
let testStart = new Date();
beforeEach(() => {
  cy.fixture("auth").then((data) => {
    userdata = data;
  });
  cy.fixture("auth").then((data) => {
    variable = data;
  });
  cy.fixture("translate").then((data) => {
    bouton = data.buttons;
  });
});

Cypress.Commands.add("signIn", (username, password) => {
  cy.clearToken();
  cy.visit("/");
  cy.location("pathname", { timeout: 60000 }).should("eq", "/login");
  cy.getByDataCy("username").type(username);
  cy.getByDataCy("password").type(password);
  cy.contains("Sign in").click();
});

Cypress.Commands.add("signInAsManager", (username, password) => {
  cy.clearToken();
  cy.visit("/");

  cy.location("pathname", { timeout: 60000 }).should("eq", "/login");
  cy.getByDataCy("username").type(username);
  cy.getByDataCy("password").type(password);
  cy.contains(bouton.SignIn).click();
  cy.location("pathname").should("eq", "/management/competitions");
  cy.getCookie("__access_token__").should("exist");
});

Cypress.Commands.add("signInAsCoach", (username, password) => {
  cy.clearToken();
  cy.visit("/");

  cy.location("pathname", { timeout: 60000 }).should("eq", "/login");

  cy.getByDataCy("username").type(userdata.coach.username);
  cy.getByDataCy("password").type(userdata.coach.password);
  cy.contains(bouton.SignIn).click();
  cy.location("pathname").should("eq", "/management/competitions");
  cy.getCookie("__access_token__").should("exist");
});

Cypress.Commands.add("signInAsPlayer", (username, password) => {
  cy.clearToken();
  cy.visit("/");

  cy.location("pathname", { timeout: 60000 }).should("eq", "/login");
  cy.getByDataCy("username").type(userdata.player.username);
  cy.getByDataCy("password").type(userdata.player.password);
  cy.contains(bouton.SignIn).click();
  cy.location("pathname").should("eq", "/competitions");
  cy.getCookie("__access_token__").should("exist");
});

Cypress.Commands.add("signInAsAdmin", () => {
  cy.clearToken();
  cy.visit("https://qa.olympiqs.com");
  cy.location("pathname", { timeout: 60000 }).should("eq", "/login");
  cy.getByDataCy("username").type(userdata.admin.username);
  cy.getByDataCy("password").type(userdata.admin.password);
  cy.contains(bouton.SignIn).click();
  cy.url().should("eq", "https://qa.olympiqs.com/administration/organizations");
  cy.getCookie("__access_token__").should("exist");
});

Cypress.Commands.add("forgetPasswordLink", () => {
  cy.clearToken();
  cy.visit("/");
  cy.location("pathname", { timeout: 60000 }).should("eq", "/login");
  cy.contains("Forgot password ?").click();
  cy.location("pathname", { timeout: 60000 }).should("eq", "/forgot-password");
  cy.getByDataCy("email").type(userdata.forgetPassword.username);
  cy.contains("Submit").click();
  cy.mailosaurGetMessage(
    variable.forgetPasswordVariable.serverID,
    { sentTo: variable.forgetPasswordVariable.emailAddress },
    {
      receivedAfter: testStart,
    }
  ).then((email) => {
    expect(email.subject).to.equal("Reset password");
    let resetLink = email.html.links[0].href;
    cy.visit(resetLink);
  });
});
