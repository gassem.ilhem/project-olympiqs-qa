Cypress.Commands.add('navigateToUsersPage', ()=>{
    cy.get('.z-50').click();
    cy.xpath("/html/body/div[1]/div[1]/div[3]/nav/a[6]").click(); 
    cy.location("pathname",{timeout:60000}).should("eq", '/management/users');

  })
  Cypress.Commands.add('navigateToChallengesPage', ()=>{
    cy.get('.z-50').click();
    cy.xpath("/html/body/div[1]/div[1]/div[3]/nav/a[4]").click(); 
    cy.location("pathname",{timeout:60000}).should("eq", '/management/challenges');

  })
  Cypress.Commands.add('navigateToCommunitiesPage', ()=>{
    cy.get('.z-50').click();
    cy.xpath("/html/body/div[1]/div[1]/div[3]/nav/a[5]").click(); 
    cy.location("pathname",{timeout:60000}).should("eq", '/management/communities');

  })
  Cypress.Commands.add('navigateToOrganizationPage', ()=>{
    cy.get('.z-50').click();
    cy.xpath("/html/body/div[1]/div[1]/div[3]/nav/a[7]").click(); 
    cy.location("pathname",{timeout:60000}).should("eq", '/management/organization');

  })

  Cypress.Commands.add('navigateToSkillsPage', ()=>{
    cy.get('.z-50').click();
    cy.xpath("/html/body/div[1]/div[1]/div[3]/nav/a[3]").click(); 
    cy.location("pathname",{timeout:60000}).should("eq", '/management/skills');

  })

  Cypress.Commands.add('navigateToTagsPage', ()=>{
    cy.get('.z-50').click();
    cy.xpath("/html/body/div[1]/div[1]/div[3]/nav/a[9]").click(); 
    cy.location("pathname",{timeout:60000}).should("eq", '/management/tags');

  })

  Cypress.Commands.add('navigateToLogsPage', ()=>{
    cy.get('.z-50').click();
    cy.xpath("/html/body/div[1]/div[1]/div[3]/nav/a[10]").click(); 
    cy.location("pathname",{timeout:60000}).should("eq", '/management/logs');

  })

  Cypress.Commands.add('navigateToProfilePage', ()=>{
    cy.get('.z-50').click();
    cy.xpath("/html/body/div[1]/div[1]/div[3]/nav/a[3]").click(); 
    cy.location("pathname",{timeout:60000}).should("eq", '/profile');

  })
  Cypress.Commands.add('navigateToCommunityPage', ()=>{
    cy.get('.z-50').click();
    cy.xpath("/html/body/div[1]/div[1]/div[3]/nav/a[2]").click(); 
    cy.location("pathname",{timeout:60000}).should("eq", '/communities/me');

  })