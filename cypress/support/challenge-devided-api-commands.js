//create quiz challenge api
Cypress.Commands.add("createQuizChallengeApi", (body) => {
  // Retrieve the access token from the cookie
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;

    // Make the API request using the obtained access token
    cy.request({
      method: "POST",
      url: "https://default.qa.olympiqs.io/api/quiz/management/",
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json",
      },
      body: body,
    }).then((response) => {
      // Assertions
      expect(response.status).to.eq(200);
    });
  });
});

///*** */
//create codeeval challenge api
Cypress.Commands.add("createCodeEvalChallengeApi", (body) => {
  // Retrieve the access token from the cookie
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;

    // Make the API request using the obtained access token
    cy.request({
      method: "POST",
      url: "https://default.qa.olympiqs.io/api/code-eval/",
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json",
      },
      body: body,
    }).then((response) => {
      // Assertions
      expect(response.status).to.eq(200);
    });
  });
});

//create challenge draft eval api
Cypress.Commands.add("createDraftChallengeApi", (body) => {
  // Retrieve the access token from the cookie
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;

    // Make the API request using the obtained access token
    cy.request({
      method: "POST",
      url: "https://default.qa.olympiqs.io/api/challenges/draft",
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json",
      },
      body: body,
    }).then((response) => {
      // Assertions
      expect(response.status).to.eq(204);
    });
  });
});

//create quiz challenge api
const nameChallenge = `challenge-${Date.now()}`;
Cypress.Commands.add("createChallengeQuizByApiWithoutBody", () => {
  cy.fixture("Api/challenge-quiz-api.json").then((quiz) => {
    cy.createSkillApi().then(function (responseSkill) {
      cy.createQuizChallengeApi({
        ...quiz,
        skills: [responseSkill.body],

        title: nameChallenge,

        questions: quiz.questions.map((question) => {
          return { ...question, skills: [responseSkill.body] };
        }),
      });
      return cy.wrap(nameChallenge).as("nameChallenge");
    });
  });
});

//create codeeval challenge api
Cypress.Commands.add("createChallengeCodeevalByApiWithoutBody", () => {
  cy.fixture("Api/challenge-codeeval-api.json").then((quiz) => {
    cy.createSkillApi().then(function (responseSkill) {
      cy.createCodeEvalChallengeApi({
        ...quiz,
        skills: [responseSkill.body], // Assign the created skill ID
        title: nameChallenge,
        snippets: quiz.snippets.map((snippet) => {
          return {
            language: {
              id: snippet.language.id,
            },
            sourceCode: snippet.sourceCode,
          };
        }),
        testCases: quiz.solution.testCases,
      });
      return cy.wrap(nameChallenge).as("nameChallenge");
    });
  });
});

//create codeeval draft challenge api
Cypress.Commands.add(
  "createDraftChallengeCodeEvalByApiWithoutBody",
  (nameChallenge) => {
    cy.fixture("Api/challenge-draft-codeeval-api.json").then((quiz) => {
      cy.createSkillApi().then(function (responseSkill) {
        cy.createDraftChallengeApi({
          ...quiz,
          skills: [responseSkill.body], // Assign the created skill ID
          title: nameChallenge,
          snippets: quiz.snippets.map((snippet) => {
            return {
              language: {
                id: snippet.language.id,
              },
              sourceCode: snippet.sourceCode,
            };
          }),
          testCases: quiz.solution.testCases,
        }).then((response) => {
          return response; // Return the entire response object
        });
      });
    });
  }
);

//create draft quiz challenge api

Cypress.Commands.add("createdraftChallengeQuizByApiWithoutBody", () => {
  cy.fixture("Api/challenge-draft-quiz-api.json").then((quiz) => {
    cy.createSkillApi().then(function (responseSkill) {
      cy.createDraftChallengeApi({
        ...quiz,
        skills: [responseSkill.body],

        title: nameChallenge,

        questions: quiz.questions.map((question) => {
          return { ...question, skills: [responseSkill.body] };
        }),
      });
      return cy.wrap(nameChallenge).as("nameChallenge");
    });
  });
});
