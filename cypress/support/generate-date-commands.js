import { addMinutes, addDays, format, addMonths } from "date-fns";
Cypress.Commands.add("generateSessionDate", (sessionBoundary = "start") => {
  const now = new Date();
  const futureDate =
    sessionBoundary === "start" ? addMinutes(now, 1) : addMonths(now, 1);
  return format(futureDate, "yyyy-MM-dd'T'HH:mm");
});

Cypress.Commands.add(
  "generateSessionDatePlusOne",
  (sessionBoundary = "start") => {
    const now = new Date();
    const futureDate =
      sessionBoundary === "start" ? addMinutes(now, 2) : addMonths(now, 1);
    return format(futureDate, "yyyy-MM-dd'T'HH:mm");
  }
);
Cypress.Commands.add(
  "generateChallengeStartDate",
  (sessionBoundary = "start") => {
    const now = new Date();
    const futureDate =
      sessionBoundary === "start" ? addMinutes(now, 50) : addMonths(now, 1);
    return format(futureDate, "yyyy-MM-dd'T'HH:mm");
  }
);

Cypress.Commands.add(
  "generateBeforeChallengeStartDate",
  (sessionBoundary = "start") => {
    const now = new Date();
    const futureDate =
      sessionBoundary === "start" ? addDays(now, -3) : addMonths(now, 1);
    return format(futureDate, "yyyy-MM-dd'T'HH:mm");
  }
);

Cypress.Commands.add(
  "generateChallengeEndDate",
  (sessionBoundary = "start") => {
    const now = new Date();
    const futureDate =
      sessionBoundary === "start" ? addMinutes(now, 20) : addMonths(now, 1);
    return format(futureDate, "yyyy-MM-dd'T'HH:mm");
  }
);

Cypress.Commands.add(
  "generateChallengeBeforeEndDate",
  (sessionBoundary = "start") => {
    const now = new Date();
    const futureDate =
      sessionBoundary === "start" ? addMinutes(now, 10) : addMonths(now, 1);
    return format(futureDate, "yyyy-MM-dd'T'HH:mm");
  }
);

Cypress.Commands.add("generateSessionEndDate", (sessionBoundary = "end") => {
  const now = new Date();
  const futureDate =
    sessionBoundary === "end" ? addMonths(now, 1) : addMinutes(now, 40);
  return format(futureDate, "yyyy-MM-dd'T'HH:mm");
});

Cypress.Commands.add("generateCurrentDate", (sessionBoundary = "end") => {
  const now = new Date();
  const futureDate =
    sessionBoundary === "end" ? addMinutes(now, 0) : addMonths(now, 0);
  return format(futureDate, "yyyy-MM-dd'T'HH:mm");
});

Cypress.Commands.add(
  "generateCurrentDatePlusOne",
  (sessionBoundary = "end") => {
    const now = new Date();
    const futureDate =
      sessionBoundary === "end" ? addMinutes(now, -1) : addMonths(now, 0);
    return format(futureDate, "yyyy-MM-dd'T'HH:mm");
  }
);

Cypress.Commands.add(
  "generateBeforeCurrentDate",
  (sessionBoundary = "start") => {
    const now = new Date();
    const futureDate =
      sessionBoundary === "start" ? addMonths(now, -1) : addMinutes(now, 1);
    return format(futureDate, "yyyy-MM-dd'T'HH:mm");
  }
);
