import { generateRandomName, colorCode } from "../utils/helper";

Cypress.Commands.add("createSkillApi", () => {
  const { nameSkill } = generateRandomName();
  const color = `#ff${colorCode}`;
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    cy.request({
      method: "POST",
      url: "https://default.qa.olympiqs.io/api/skills/",
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json",
      },
      body: { name: nameSkill, color: color },
    }).then((response) => {
      expect(response.status).to.eq(200);
    });
  });
});
