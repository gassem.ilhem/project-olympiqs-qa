import { generateRandomName } from "../utils/helper";
//get tag
Cypress.Commands.add("getTagApi", (id) => {
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    cy.request({
      method: "PATCH",
      url: `https://default.qa.olympiqs.io/api/tags/${id}`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json",
      },
    }).then((response) => {
      expect(response.status).to.eq(201);
    });
  });
});

Cypress.Commands.add("createTagApi", () => {
  const { nameTag } = generateRandomName();
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    cy.request({
      method: "POST",
      url: "https://default.qa.olympiqs.io/api/tags/",
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json",
      },
      body: {
        name: nameTag,
      },
    }).then((response) => {
      expect(response.status).to.eq(201);
    });
  });
});

Cypress.Commands.add("createTag", () => {
  const { nameTag } = generateRandomName();
  cy.getCookie("__access_token__").then(function (accessToken) {
    cy.request({
      method: "POST",
      url: "/api/tags/",
      body: { name: nameTag },
      headers: {
        Authorization: accessToken.value,
        "Content-Type": "application/json",
      },
    }).then((response) => {
      expect(response.isOkStatusCode).to.equal(true);
    });
  });
});
