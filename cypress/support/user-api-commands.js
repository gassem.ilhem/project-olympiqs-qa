import {
  generateRandomName,
  generateRandomEmail,
  generatePassword,
} from "../utils/helper";

//get user
Cypress.Commands.add("getUserApi", (email) => {
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    cy.request({
      method: "GET",
      url: "https://default.qa.olympiqs.io/api/users/?criteria[term]=" + email,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json",
      },
    }).then((response) => {
      expect(response.status).to.eq(200);
    });
  });
});

Cypress.Commands.add("createManagerApi", () => {
  const { firstName, lastName } = generateRandomName();
  const email = generateRandomEmail("manager");
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    cy.request({
      method: "POST",
      url: "https://qa.olympiqs.com/api/organizations/660e8f4ee01053c4200c8f63/users/organization-manager",
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json",
      },
      body: [
        {
          firstName: firstName,
          lastName: lastName,
          email: email,
          organization: "default",
        },
      ],
    }).then((response) => {
      expect(response.status).to.eq(201);
    });
  });
});
//create manager with mail
Cypress.Commands.add("createManagerApiWithoutEmail", (randomEmail) => {
  const { firstName, lastName } = generateRandomName();
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    cy.request({
      method: "POST",
      url: "https://qa.olympiqs.com/api/organizations/660e8f4ee01053c4200c8f63/users/organization-manager",
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json",
      },
      body: [
        {
          firstName: firstName,
          lastName: lastName,
          email: randomEmail,
          organization: "default",
        },
      ],
    }).then((response) => {
      expect(response.status).to.eq(201);
    });
  });
});

Cypress.Commands.add("createCoachApi", () => {
  const { firstName, lastName } = generateRandomName();
  const email = generateRandomEmail("coach");
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    cy.createCommunityApi().then(function (responseCommunity) {
      cy.request({
        method: "POST",
        url: "https://default.qa.olympiqs.io/api/organizations/users/coaches",
        headers: {
          Authorization: `Bearer ${accessToken}`,
          "Content-Type": "application/json",
        },
        body: [
          {
            firstName: firstName,
            lastName: lastName,
            email: email,
            communities: [responseCommunity.body.id],
          },
        ],
      }).then((response) => {
        expect(response.status).to.eq(201);
      });
    });
  });
});

Cypress.Commands.add("createPlayerApi", (firstName, lastName, email) => {
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    cy.createTagApi().then(function (responseTag) {
      cy.request({
        method: "POST",
        url: "https://default.qa.olympiqs.io/api/organizations/users/players",
        headers: {
          Authorization: `Bearer ${accessToken}`,
          "Content-Type": "application/json",
        },
        body: [
          {
            firstName: firstName,
            lastName: lastName,
            email: email,
            communities: [],
            tags: [responseTag.body.id],
          },
        ],
      }).then((response) => {
        expect(response.status).to.eq(201);
      });
    });
  });
});
//create player with tag outside
Cypress.Commands.add("createPlayerApiOutTag", (responseTag) => {
  const { firstName, lastName } = generateRandomName();
  const email = generateRandomEmail("player");
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    cy.request({
      method: "POST",
      url: "https://default.qa.olympiqs.io/api/organizations/users/players",
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json",
      },
      body: [
        {
          firstName: firstName,
          lastName: lastName,
          email: email,
          communities: [],
          tags: [responseTag],
        },
      ],
    }).then((response) => {
      expect(response.status).to.eq(201);
    });
  });
});
//create player with community
Cypress.Commands.add("createPlayerWithCommunityApi", (responseCommunity) => {
  const { firstName, lastName } = generateRandomName();
  const email = generateRandomEmail("player");
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    cy.createTagApi().then(function (responseTag) {
      cy.request({
        method: "POST",
        url: "https://default.qa.olympiqs.io/api/organizations/users/players",
        headers: {
          Authorization: `Bearer ${accessToken}`,
          "Content-Type": "application/json",
        },
        body: [
          {
            firstName: firstName,
            lastName: lastName,
            email: email,
            communities: [responseCommunity],
            tags: [responseTag.body.id],
          },
        ],
      }).then((response) => {
        expect(response.status).to.eq(201);
      });
    });
  });
});

//activate player pending ( create password )
Cypress.Commands.add("activatePlayerPending", (id) => {
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    const requestBody = {
      plainPassword: "Ilhem$98",
      confirmPlainPassword: "Ilhem$98",
    };
    cy.request({
      method: "PATCH",
      url: `https://default.qa.olympiqs.io/api/users/${id}/password-creation`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json",
      },
      body: requestBody,
    }).then((response) => {
      expect(response.status).to.eq(204);
    });
  });
});
//activate player pending ( create password ) generate password
Cypress.Commands.add(
  "activatePlayerPendingPassword",
  (id, plainPassword, confirmPlainPassword) => {
    cy.getCookie("__access_token__").then((cookie) => {
      if (!cookie) {
        throw new Error(
          "Access token not found in cookie. Please make sure to sign in first."
        );
      }
      const accessToken = cookie.value;
      cy.request({
        method: "PATCH",
        url: `https://default.qa.olympiqs.io/api/users/${id}/password-creation`,
        headers: {
          Authorization: `Bearer ${accessToken}`,
          "Content-Type": "application/json",
        },
        body: {
          plainPassword,
          confirmPlainPassword,
        },
      }).then((response) => {
        expect(response.status).to.eq(204);
      });
    });
  }
);

//desactivate player
Cypress.Commands.add("desactivatePlayer", (id) => {
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    cy.request({
      method: "GET",
      url: `https://default.qa.olympiqs.io/api/users/${id}/toggle-status`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json",
      },
    }).then((response) => {
      expect(response.isOkStatusCode).to.equal(true);
    });
  });
});
Cypress.Commands.add("InvitePlayer", (email) => {
  const { firstName, lastName } = generateRandomName();
  cy.clearToken();
  cy.fixture("auth").then((data) => {
    const userdata = data.manager;
    cy.loginAPI(userdata.username, userdata.password);
    cy.createPlayerApi(firstName, lastName, email).then((player) => {
      cy.getUserApi(player.body.data[0].email).then((playerAct) => {});
    });
  });
});
