import { generateRandomName } from "../utils/helper";
Cypress.Commands.add("createCommunityApi", () => {
  const { nameCommunity, description } = generateRandomName();
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    cy.fixture("manager").then((manager) => {
      cy.request({
        method: "POST",
        url: "https://default.qa.olympiqs.io/api/communities/",
        headers: {
          Authorization: `Bearer ${accessToken}`,
          "Content-Type": "application/json",
        },
        body: {
          name: nameCommunity,
          description: description,
          logo: null,
          email: manager.email,
          managers: [manager],
        },
      }).then((response) => {
        expect(response.status).to.eq(200);
      });
    });
  });
});
