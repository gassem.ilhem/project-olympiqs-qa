import { generateRandomName } from "../utils/helper";

//get challenge
Cypress.Commands.add("getChallengeApi", (id) => {
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    cy.request({
      method: "PATCH",
      url: `https://default.qa.olympiqs.io/api/challenges/${id}`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json",
      },
    }).then((response) => {
      expect(response.status).to.eq(200);
    });
  });
});

//deactivate challenge
Cypress.Commands.add("desactivateChallenge", (id) => {
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    cy.request({
      method: "PUT",
      url: `https://default.qa.olympiqs.io/api/challenges/${id}/toggle-status`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json",
      },
    }).then((response) => {
      expect(response.status).to.eq(200);
    });
  });
});

//desactivate challenge draft eval api
Cypress.Commands.add("desactivateDraftChallengeApi", (body) => {
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    cy.createCodeEvalChallengeExemple().then(function (firstResponse) {
      console.log("first response : ", firstResponse.body.id);
      const challengeId = firstResponse.body.id;
      const challengeTitle = firstResponse.title;
      console.log("first response id : ", challengeId);
      cy.request({
        method: "PUT",
        url: `https://default.qa.olympiqs.io/api/challenges/${challengeId}/toggle-status`,
        headers: {
          Authorization: `Bearer ${accessToken}`,
          "Content-Type": "application/json",
        },
      }).then((response) => {
        expect(response.status).to.eq(200);
      });
    });
  });
});

Cypress.Commands.add("createQuizChallengeApiExemple", () => {
  const { nameChallenge } = generateRandomName();
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    cy.fixture("Api/challenge-quiz-api.json").then((quiz) => {
      cy.createSkillApi().then(function (responseSkill) {
        const createQuizChallenge = {
          ...quiz,
          skills: [responseSkill.body],
          title: nameChallenge,
          questions: quiz.questions.map((question) => {
            return { ...question, skills: [responseSkill.body] };
          }),
        };
        cy.request({
          method: "POST",
          url: "https://default.qa.olympiqs.io/api/quiz/management/",
          headers: {
            Authorization: `Bearer ${accessToken}`,
            "Content-Type": "application/json",
          },
          body: createQuizChallenge,
        }).then((response) => {
          expect(response.status).to.eq(200);
        });
      });
    });
  });
});
//create quiz challenge type competition api
Cypress.Commands.add("createQuizChallengeApiExemple", () => {
  const { nameChallenge } = generateRandomName();
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    cy.fixture("Api/challenge-quiz-api.json").then((quiz) => {
      cy.createSkillApi().then(function (responseSkill) {
        const createQuizChallenge = {
          ...quiz,
          skills: [responseSkill.body],
          title: nameChallenge,
          questions: quiz.questions.map((question) => {
            return { ...question, skills: [responseSkill.body] };
          }),
        };
        cy.request({
          method: "POST",
          url: "https://default.qa.olympiqs.io/api/quiz/management/",
          headers: {
            Authorization: `Bearer ${accessToken}`,
            "Content-Type": "application/json",
          },
          body: createQuizChallenge,
        }).then((response) => {
          // Assertions
          expect(response.status).to.eq(200);
        });
      });
    });
  });
});
//create quiz challenge type competition api lost
Cypress.Commands.add("createQuizChallengeApiLost", () => {
  const { nameChallenge } = generateRandomName();
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    cy.fixture("Api/challenge-quiz-lost-api.json").then((quiz) => {
      cy.createSkillApi().then(function (responseSkill) {
        const createQuizChallenge = {
          ...quiz,
          skills: [responseSkill.body],
          title: nameChallenge,
          questions: quiz.questions.map((question) => {
            return { ...question, skills: [responseSkill.body] };
          }),
        };
        cy.request({
          method: "POST",
          url: "https://default.qa.olympiqs.io/api/quiz/management/",
          headers: {
            Authorization: `Bearer ${accessToken}`,
            "Content-Type": "application/json",
          },
          body: createQuizChallenge,
        }).then((response) => {
          expect(response.status).to.eq(200);
        });
      });
    });
  });
});

//create quiz challenge type competition api
Cypress.Commands.add("createQuizChallengeApiTimeout", () => {
  const { nameChallenge } = generateRandomName();
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    cy.fixture("Api/challenge-quiz-api-timeout.json").then((quiz) => {
      cy.createSkillApi().then(function (responseSkill) {
        const createQuizChallenge = {
          ...quiz,
          skills: [responseSkill.body],
          title: nameChallenge,
          questions: quiz.questions.map((question) => {
            return { ...question, skills: [responseSkill.body] };
          }),
        };
        cy.request({
          method: "POST",
          url: "https://default.qa.olympiqs.io/api/quiz/management/",
          headers: {
            Authorization: `Bearer ${accessToken}`,
            "Content-Type": "application/json",
          },
          body: createQuizChallenge,
        }).then((response) => {
          expect(response.status).to.eq(200);
        });
      });
    });
  });
});
Cypress.Commands.add("createQuizChallengeApiGenerateName", (nomChallenge) => {
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    cy.fixture("Api/challenge-quiz-api.json").then((quiz) => {
      cy.createSkillApi().then(function (responseSkill) {
        const createQuizChallenge = {
          ...quiz,
          skills: [responseSkill.body],
          title: nomChallenge,
          displayDefaultInstructions: true,
          questions: quiz.questions.map((question) => {
            return { ...question, skills: [responseSkill.body] };
          }),
        };
        cy.request({
          method: "POST",
          url: "https://default.qa.olympiqs.io/api/quiz/management/",
          headers: {
            Authorization: `Bearer ${accessToken}`,
            "Content-Type": "application/json",
          },
          body: createQuizChallenge,
        }).then((response) => {
          expect(response.status).to.eq(200);
        });
      });
    });
  });
});

//create quiz challenge type training api
Cypress.Commands.add("createQuizChallengeTrainingApiExemple", () => {
  const { nameChallenge } = generateRandomName();
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    cy.fixture("Api/challenge-quiz-training-api.json").then((quiz) => {
      cy.createSkillApi().then(function (responseSkill) {
        const createQuizChallenge = {
          ...quiz,
          skills: [responseSkill.body],
          title: nameChallenge,
          questions: quiz.questions.map((question) => {
            return { ...question, skills: [responseSkill.body] };
          }),
        };
        cy.request({
          method: "POST",
          url: "https://default.qa.olympiqs.io/api/quiz/management/",
          headers: {
            Authorization: `Bearer ${accessToken}`,
            "Content-Type": "application/json",
          },
          body: createQuizChallenge,
        }).then((response) => {
          expect(response.status).to.eq(200);
        });
      });
    });
  });
});

Cypress.Commands.add("createCodeEvalChallengeExemple", () => {
  const { nameChallenge } = generateRandomName();
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;

    cy.fixture("Api/challenge-codeeval-api.json").then((payload) => {
      payload.title = nameChallenge;
      cy.createSkillApi().then(function (responseSkill) {
        (payload.skills = [responseSkill.body]),
          cy
            .request({
              method: "POST",
              url: "https://default.qa.olympiqs.io/api/code-eval/",
              headers: {
                Authorization: `Bearer ${accessToken}`,
                "Content-Type": "application/json",
              },
              body: payload,
            })
            .then((response) => {
              expect(response.status).to.eq(200);
            });
      });
    });
  });
});

Cypress.Commands.add("createCodeEvalChallengeTimeout", () => {
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    cy.fixture("Api/challenge-codeeval-api-timeout.json").then((payload) => {
      payload.title = nameChallenge;
      cy.createSkillApi().then(function (responseSkill) {
        (payload.skills = [responseSkill.body]),
          cy
            .request({
              method: "POST",
              url: "https://default.qa.olympiqs.io/api/code-eval/",
              headers: {
                Authorization: `Bearer ${accessToken}`,
                "Content-Type": "application/json",
              },
              body: payload,
            })
            .then((response) => {
              expect(response.status).to.eq(200);
            });
      });
    });
  });
});

// Create a draft challenge via API
Cypress.Commands.add("createDraftChallengeQuizApiExemple", () => {
  const { nameChallenge } = generateRandomName();
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    cy.fixture("Api/challenge-draft-quiz-api.json").then((quiz) => {
      cy.createSkillApi().then(function (responseSkill) {
        const challengeDraft = {
          ...quiz,
          skills: [responseSkill.body],
          title: nameChallenge,
        };
        cy.request({
          method: "POST",
          url: "https://default.qa.olympiqs.io/api/challenges/draft",
          headers: {
            Authorization: `Bearer ${accessToken}`,
            "Content-Type": "application/json",
          },
          body: challengeDraft,
        }).then((response) => {
          expect(response.status).to.eq(204);
        });
      });
    });
  });
});

// Create a draft challenge via API
Cypress.Commands.add("createDraftChallengeCodeevalApiExemple", () => {
  const { nameChallenge } = generateRandomName();
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    cy.fixture("Api/challenge-draft-codeeval-api.json").then((quiz) => {
      cy.createSkillApi().then(function (responseSkill) {
        const challengeDraft = {
          ...quiz,
          skills: [responseSkill.body],
          title: nameChallenge,
        };
        cy.request({
          method: "POST",
          url: "https://default.qa.olympiqs.io/api/challenges/draft",
          headers: {
            Authorization: `Bearer ${accessToken}`,
            "Content-Type": "application/json",
          },
          body: challengeDraft,
        }).then((response) => {
          expect(response.status).to.eq(204);
        });
      });
    });
  });
});

//create codeeval draft challenge api
Cypress.Commands.add(
  "createDraftChallengeCodeEvalByApiWithoutBody",
  (nomChallenge) => {
    cy.fixture("Api/challenge-draft-codeeval-api.json").then((quiz) => {
      cy.createSkillApi().then(function (responseSkill) {
        cy.createDraftChallengeApi({
          ...quiz,
          skills: [responseSkill.body],
          title: nomChallenge,
          snippets: quiz.snippets.map((snippet) => {
            return {
              language: {
                id: snippet.language.id,
              },
              sourceCode: snippet.sourceCode,
            };
          }),
          testCases: quiz.solution.testCases,
        }).then((response) => {
          return response;
        });
      });
    });
  }
);
