Cypress.Commands.add('generatePassword', async () => {
    const firstPart = 'Ilhem';
    const specialCharacters = '!@#$%^&*';
    const numbers = '0123456789';

    // Helper function to get a random character from a string
    const getRandomChar = (characters) => {
        const randomIndex = Math.floor(Math.random() * characters.length);
        return characters.charAt(randomIndex);
    };

    // Generate the random parts of the password
    const specialChar = getRandomChar(specialCharacters);
    const randomNumber1 = getRandomChar(numbers);
    const randomNumber2 = getRandomChar(numbers);

    // Concatenate the parts to form the password
    const generatedPassword = `${firstPart}${specialChar}${randomNumber1}${randomNumber2}`;

    return generatedPassword;
});