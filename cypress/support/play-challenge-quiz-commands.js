Cypress.Commands.add("singleEntry", (content) => {
    cy.contains(content).click({
      force: true,
    });
    cy.wait(2000);
  });
  Cypress.Commands.add("multiEntries", (options) => {
    options.forEach((option) => {
      cy.contains(option.content).click({
        force: true,
      });
    });
    cy.wait(2000);
  });
  Cypress.Commands.add("textEntry", (answers) => {
    answers.forEach((answer) => {
      cy.get('[data-mode="text-entry"]').type(answer);
    });
    cy.wait(2000);
  });
  Cypress.Commands.add("multiTextEntries", (answers) => {
    answers.forEach((answer, index) => {
      cy.get(`input#${index + 1}`).type(answer);
    });
    cy.wait(2000);
  });


  Cypress.Commands.add("confirmAnswers", (index, lengthQuestion) => {
    if (index === lengthQuestion) {
      cy.contains("Validate").click({ force: true });
  
      cy.contains("Submit").click({ force: true });
  
      cy.wait(1000);
    } else {
      cy.contains("Next").click({ force: true });
    }
  });