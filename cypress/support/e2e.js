// ***********************************************************
// This example support/e2e.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import "./commands";
import "./login-api-commands";
import "./login-ui-commands";
import "./challenge-api-commands";
import "./community-api-commands";
import "./session-api-commands";
import "./skill-api-commands";
import "./tags-api-commands";
import "./user-api-commands";
import "./generate-date-commands";
import "./navigate-to-pages";
import "./challenge-devided-api-commands";
import "./generate-password";
import "./play-challenge-quiz-commands";
import setLightTheme from "cypress-light-theme";
import "cypress-mochawesome-reporter/register";
setLightTheme();
// Alternatively you can use CommonJS syntax:
// require('./commands')
