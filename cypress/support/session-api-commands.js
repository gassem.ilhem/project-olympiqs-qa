import {
  generateRandomName,
  generateSessionDate,
  generateSessionDateGMT,
} from "../utils/helper";
const sessionyName = `session-${Date.now()}`;

Cypress.Commands.add("createSessionCompetitionApi", () => {
  const { nameSession } = generateRandomName();
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    cy.fixture("Api/session-competition-api.json").then((sessionJson) => {
      cy.createCommunityApi().then(function (responseCommunity) {
        cy.createQuizChallengeApiExemple().then(function (responseChallenge) {
          const startedAtTime = generateSessionDate();
          const endedAtTime = generateSessionDate("end");
          const session = {
            ...sessionJson,
            challengeContext: {
              type: "standard",
              community: responseCommunity.body,
              description: "helllo",
              title: nameSession,
              startedAt: startedAtTime,
              endedAt: endedAtTime,
            },
            challenges: sessionJson.challenges.map((challenge) => ({
              ...challenge,
              startedAt: startedAtTime,
              endedAt: endedAtTime,
              monitored: true,
              showCorrectAnswers: false,
              challenge: responseChallenge.body.id,
              title: responseChallenge.body.title,
            })),
          };
          cy.request({
            method: "POST",
            url: "https://default.qa.olympiqs.io/api/challenge-contexts/",
            headers: {
              Authorization: `Bearer ${accessToken}`,
              "Content-Type": "application/json",
            },
            body: session,
          }).then((response) => {
            expect(response.status).to.eq(200);
          });
        });
      });
    });
  });
});

//create session training api
Cypress.Commands.add("createSessiontrainingApi", () => {
  const { nameSession } = generateRandomName();
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    cy.fixture("Api/session-competition-api.json").then((sessionJson) => {
      cy.createCommunityApi().then(function (responseCommunity) {
        cy.createQuizChallengeTrainingApiExemple().then(function (
          responseChallenge
        ) {
          cy.generateSessionDate().then((sessionDate) => {
            cy.generateSessionEndDate().then((sessionEndDate) => {
              const session = {
                ...sessionJson,
                challengeContext: {
                  community: responseCommunity.body,
                  description: "helllo",
                  title: nameSession,
                  startedAt: sessionDate + ":00.000Z",
                  endedAt: sessionEndDate + ":00.000Z",
                },
                challenges: sessionJson.challenges.map((challenge) => ({
                  ...challenge,
                  startedAt: sessionDate + ":00.000Z",
                  endedAt: sessionEndDate + ":00.000Z",
                  monitored: true,
                  showCorrectAnswers: false,
                  challenge: responseChallenge.body.id,
                  title: responseChallenge.body.title,
                })),
              };
              cy.request({
                method: "POST",
                url: "https://default.qa.olympiqs.io/api/challenge-contexts/",
                headers: {
                  Authorization: `Bearer ${accessToken}`,
                  "Content-Type": "application/json",
                },
                body: session,
              }).then((response) => {
                expect(response.status).to.eq(200);
              });
            });
          });
        });
      });
    });
  });
});

//create session finished api
Cypress.Commands.add("createSessionCompetitionFinishedApi", () => {
  const { nameSession } = generateRandomName();
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    cy.fixture("Api/session-competition-api.json").then((sessionJson) => {
      cy.createCommunityApi().then(function (responseCommunity) {
        cy.createQuizChallengeApiExemple().then(function (responseChallenge) {
          cy.generateCurrentDate().then((sessionDate) => {
            cy.generateCurrentDatePlusOne().then((sessionEndDate) => {
              const session = {
                ...sessionJson,
                challengeContext: {
                  community: responseCommunity.body,
                  description: "helllo",
                  title: nameSession,
                  startedAt: sessionDate + ":00.000Z",
                  endedAt: sessionEndDate + ":00.000Z",
                },
                challenges: sessionJson.challenges.map((challenge) => ({
                  ...challenge,
                  startedAt: sessionDate + ":00.000Z",
                  endedAt: sessionEndDate + ":00.000Z",
                  monitored: true,
                  showCorrectAnswers: false,
                  challenge: responseChallenge.body.id,
                  title: responseChallenge.body.title,
                })),
              };
              cy.request({
                method: "POST",
                url: "https://default.qa.olympiqs.io/api/challenge-contexts/",
                headers: {
                  Authorization: `Bearer ${accessToken}`,
                  "Content-Type": "application/json",
                },
                body: session,
              }).then((response) => {
                expect(response.status).to.eq(200);
              });
            });
          });
        });
      });
    });
  });
});

//create session inprogress api
Cypress.Commands.add("createSessionCompetitionInProgressApi", () => {
  const { nameSession } = generateRandomName();
  cy.getCookie("__access_token__").then((cookie) => {
    if (!cookie) {
      throw new Error(
        "Access token not found in cookie. Please make sure to sign in first."
      );
    }
    const accessToken = cookie.value;
    const sessionStartDate = new Date();
    sessionStartDate.setSeconds(sessionStartDate.getSeconds() + 10);
    const sessionStartDateString = sessionStartDate
      .toISOString()
      .replace(/\.\d+Z$/, "+00:00");
    const sessionEndDate = new Date();
    sessionEndDate.setDate(sessionEndDate.getDate() + 1);
    const sessionEndDateString = sessionEndDate
      .toISOString()
      .replace(/\.\d+Z$/, "+00:00");
    cy.fixture("Api/session-competition-api.json").then((sessionJson) => {
      cy.createCommunityApi().then(function (responseCommunity) {
        cy.createQuizChallengeApiExemple().then(function (responseChallenge) {
          const session = {
            ...sessionJson,
            challengeContext: {
              community: responseCommunity.body,
              description: "helllo",
              title: nameSession,
              startedAt: sessionStartDateString,
              endedAt: sessionEndDateString,
            },
            challenges: sessionJson.challenges.map((challenge) => ({
              ...challenge,
              startedAt: sessionStartDateString,
              endedAt: sessionEndDateString,
              monitored: true,
              showCorrectAnswers: false,
              challenge: responseChallenge.body.id,
              title: responseChallenge.body.title,
            })),
          };
          cy.request({
            method: "POST",
            url: "https://default.qa.olympiqs.io/api/challenge-contexts/",
            headers: {
              Authorization: `Bearer ${accessToken}`,
              "Content-Type": "application/json",
            },
            body: session,
          }).then((response) => {
            expect(response.status).to.eq(200);
          });
        });
      });
    });
  });
});

//create session with communty
Cypress.Commands.add(
  "createSessionCompetitionWithCommunityApi",
  (responseCommunity) => {
    const { nameSession } = generateRandomName();
    cy.getCookie("__access_token__").then((cookie) => {
      if (!cookie) {
        throw new Error(
          "Access token not found in cookie. Please make sure to sign in first."
        );
      }
      const accessToken = cookie.value;
      cy.fixture("Api/session-competition-api.json").then((sessionJson) => {
        cy.createQuizChallengeApiExemple().then(function (responseChallenge) {
          cy.generateSessionDate().then((sessionDate) => {
            cy.generateSessionEndDate().then((sessionEndDate) => {
              const session = {
                ...sessionJson,
                challengeContext: {
                  type: "standard",
                  community: responseCommunity,
                  description: "helllo",
                  title: nameSession,
                  startedAt: sessionDate + ":00.000Z",
                  endedAt: sessionEndDate + ":00.000Z",
                },
                challenges: sessionJson.challenges.map((challenge) => ({
                  ...challenge,
                  startedAt: sessionDate + ":00.000Z",
                  endedAt: sessionEndDate + ":00.000Z",
                  monitored: true,
                  showCorrectAnswers: false,
                  challenge: responseChallenge.body.id,
                  title: responseChallenge.body.title,
                })),
              };
              cy.request({
                method: "POST",
                url: "https://default.qa.olympiqs.io/api/challenge-contexts/",
                headers: {
                  Authorization: `Bearer ${accessToken}`,
                  "Content-Type": "application/json",
                },
                body: session,
              }).then((response) => {
                expect(response.status).to.eq(200);
              });
            });
          });
        });
      });
    });
  }
);

//create session with communty adn challenge
Cypress.Commands.add(
  "createSessionCompetitionWithChallengeApi",
  (responseCommunity, responseChallengeId, responseChallengeTitle) => {
    const { nameSession } = generateRandomName();
    cy.getCookie("__access_token__").then((cookie) => {
      if (!cookie) {
        throw new Error(
          "Access token not found in cookie. Please make sure to sign in first."
        );
      }
      const accessToken = cookie.value;
      const sessionStartDate = new Date();
      sessionStartDate.setSeconds(sessionStartDate.getSeconds() + 10);
      const sessionStartDateString = sessionStartDate
        .toISOString()
        .replace(/\.\d+Z$/, "+00:00");
      // Générer la date de fin (ajouter un jour à la date actuelle)
      const sessionEndDate = new Date();
      sessionEndDate.setDate(sessionEndDate.getDate() + 1);
      const sessionEndDateString = sessionEndDate
        .toISOString()
        .replace(/\.\d+Z$/, "+00:00");
      cy.fixture("Api/session-competition-api.json").then((sessionJson) => {
        cy.generateSessionDate().then((sessionDate) => {
          cy.generateSessionEndDate().then((sessionEndDate) => {
            const session = {
              ...sessionJson,
              challengeContext: {
                type: "standard",
                community: responseCommunity,
                description: "helllo",
                title: nameSession,
                startedAt: sessionStartDateString,
                endedAt: sessionEndDateString,
              },
              challenges: sessionJson.challenges.map((challenge) => ({
                ...challenge,
                startedAt: sessionStartDateString,
                endedAt: sessionEndDateString,
                monitored: true,
                showCorrectAnswers: false,
                challenge: responseChallengeId,
                title: responseChallengeTitle,
              })),
            };
            cy.request({
              method: "POST",
              url: "https://default.qa.olympiqs.io/api/challenge-contexts/",
              headers: {
                Authorization: `Bearer ${accessToken}`,
                "Content-Type": "application/json",
              },
              body: session,
            }).then((response) => {
              expect(response.status).to.eq(200);
            });
          });
        });
      });
    });
  }
);
