import "cypress-mailosaur";
require("@cypress/xpath");

const dotenv = require("dotenv");
const path = require("path");

Cypress.Commands.add("clearToken", () => {
  cy.clearCookie("__access_token__");
});

Cypress.Commands.add("getByName", (selector) => {
  return cy.get(`[name=${selector}]`);
});

Cypress.Commands.add("getById", (selector) => {
  return cy.get(`[id=${selector}]`);
});
Cypress.Commands.add("getByDataCy", (selector) => {
  return cy.get(`[data-cy=${selector}]`);
});
Cypress.Commands.add("searchAndSubmit", (searchTerm) => {
  cy.get('input[name="term"]').type(`${searchTerm}{enter}`);
});
Cypress.Commands.add("videSearch", (searchTerm) => {
  cy.get('input[name="term"]').clear();
});
