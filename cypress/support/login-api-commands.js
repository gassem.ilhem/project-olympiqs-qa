Cypress.Commands.add("loginAdminAPI", () => {
  cy.fixture("Api/header-login-api.json").then((headers) => {
    cy.request({
      method: "POST",
      url: `https://qa.olympiqs.com/api/oauth2/access/token`,
      headers: headers,

      body: {
        client_id: Cypress.env("CLIENT_ADMIN_ID"),
        client_secret: Cypress.env("CLIENT_ADMIN_SECRET"),
        username: "admin",
        password: "admin",
        grant_type: "password",
      },
      form: true,
    }).then((response) => {
      expect(response.status).to.equal(200);
      cy.setCookie("__access_token__", response.body.access_token);
      cy.setCookie("__refresh_token__", response.body.refresh_token);
      let accessToken = "Bearer " + response.body.access_token;

      return cy.wrap(accessToken).as("accessToken");
    });
  });
});
Cypress.Commands.add("loginAPI", (username, password) => {
  cy.clearCookies();
  return cy.fixture("Api/header-login-api.json").then((headers) => {
    return cy
      .request({
        method: "POST",
        url: `https://default.qa.olympiqs.io/api/oauth2/access/token`,
        headers: headers,
        body: {
          client_id: Cypress.env("CLIENT_ID"),
          client_secret: Cypress.env("CLIENT_SECRET"),
          username: username,
          password: password,
          grant_type: "password",
        },
        form: true,
      })
      .then((response) => {
        expect(response.status).to.equal(200);
        cy.setCookie("__access_token__", response.body.access_token);
        cy.setCookie("__refresh_token__", response.body.refresh_token);
        let accessToken = "Bearer " + response.body.access_token;
        return cy.wrap(accessToken).as("accessToken");
      });
  });
});

Cypress.Commands.add("loginAdminAPI2", (username, password) => {
  cy.clearCookies();

  return cy.fixture("Api/header-login-api.json").then((headers) => {
    return cy
      .request({
        method: "POST",
        url: `https://qa.olympiqs.com/api/oauth2/access/token`,
        headers: headers,
        body: {
          client_id: Cypress.env("CLIENT_ID"),
          client_secret: Cypress.env("CLIENT_SECRET"),
          username: "admin",
          password: "admin",
          grant_type: "password",
        },
        form: true,
      })
      .then((response) => {
        expect(response.status).to.equal(200);
        cy.setCookie("__access_token__", response.body.access_token);
        cy.setCookie("__refresh_token__", response.body.refresh_token);
        let accessToken = "Bearer " + response.body.access_token;
        return cy.wrap(accessToken).as("accessToken");
      });
  });
});
