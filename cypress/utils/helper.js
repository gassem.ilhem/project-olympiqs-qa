import { v4 as uuidv4 } from "uuid";
import { addMinutes, format, addMonths, addHours } from "date-fns";
import { utcToZonedTime, format as formaTZ } from "date-fns-tz";

export function generateSessionDate(sessionBoundary = "start") {
  // Get the current date and time
  const now = new Date();
  const futureDate =
    sessionBoundary === "start" ? addMinutes(now, 1) : addMonths(now, 1);
  // Format the date as "YYYY-MM-DDTHH:mm:ss.SSS[Z]"
  return format(futureDate, "yyyy-MM-dd'T'HH:mm");
}

export function generateSessionDatePlusOne(sessionBoundary = "start") {
  // Get the current date and time
  const now = new Date();
  const futureDate =
    sessionBoundary === "start" ? addHours(now, 5) : addMonths(now, 1);

  // Format the date as "YYYY-MM-DDTHH:mm:ss.SSS[Z]"
  return format(futureDate, "yyyy-MM-dd'T'HH:mm");
}

export function generateSessionDateGMT(sessionBoundary = "start") {
  const now = new Date();
  const gmtNow = utcToZonedTime(now, "UTC");
  const futureDate =
    sessionBoundary === "start" ? addMinutes(gmtNow, 1) : addMonths(gmtNow, 1);

  return format(futureDate, "yyyy-MM-dd'T'HH:mm:ss", { timeZone: "UTC" });
}

export function generateRandomEmail(role) {
  return `${role}-${Date.now()}-${Cypress._.random(1000, 9999)}@email.com`;
}
export function generatePassword() {
  const firstPart = "Ilhem";
  const specialCharacters = "!@#$%^&*";
  const numbers = "0123456789";

  function getRandomChar(characters) {
    const randomIndex = Math.floor(Math.random() * characters.length);
    return characters.charAt(randomIndex);
  }

  // Generate the random parts of the password
  const specialChar = getRandomChar(specialCharacters);
  const randomNumber1 = getRandomChar(numbers);
  const randomNumber2 = getRandomChar(numbers);

  // Concatenate the parts to form the password
  const generatedPassword = `${firstPart}${specialChar}${randomNumber1}${randomNumber2}`;

  return generatedPassword;
}

export const password = generatePassword();

export const colorCode = Cypress._.random(9999);
export function randomNameGenerator(prefixe) {
  let name = uuidv4();

  return prefixe + name;
}
export const generateRandomName = () => {
  const nameSkill = randomNameGenerator("skill_");
  const nameSecondSkill = randomNameGenerator("skill_");
  const nameTag = randomNameGenerator("tag_");
  const nameNewTag = randomNameGenerator("tag_");

  const nameChallenge = randomNameGenerator("challenge");
  const nameSession = randomNameGenerator("session");
  const nameCommunity = randomNameGenerator("community");
  const description = randomNameGenerator("description");
  const fullName = randomNameGenerator("name");
  const firstName = randomNameGenerator("firstName");
  const lastName = randomNameGenerator("lastName");
  const newnameChallenge = randomNameGenerator("challenge");
  const newnameSession = randomNameGenerator("session");
  const newnameCommunity = randomNameGenerator("community");
  return {
    nameSkill,
    nameSecondSkill,
    nameChallenge,
    nameSession,
    nameCommunity,
    description,

    newnameSession,
    newnameCommunity,
    newnameChallenge,
    nameTag,
    nameNewTag,
    fullName,
    firstName,
    lastName,
  };
};
