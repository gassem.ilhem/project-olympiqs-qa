const { defineConfig } = require("cypress");

module.exports = defineConfig({
  projectId: "px9aes",
  reporter: "cypress-mochawesome-reporter",
  reporterOptions: {
    charts: true,
    reportPageTitle: "OlympIQs Report",
    embeddedScreenshots: true,
    inlineAssets: true,
  },
  e2e: {
    setupNodeEvents(on, config) {
      require("cypress-mochawesome-reporter/plugin")(on);
    },
    defaultCommandTimeout: 15000,
    testIsolation: false,
    baseUrl: "https://default.qa.olympiqs.io",
    env: {
      THEME: "light",
    },
  },
});
